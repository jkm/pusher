#ifndef V4L2_ACCESS
#define V4L2_ACCESS

#include "V4l2Device.h"

class V4l2Access {
public:
	V4l2Access(V4l2Device *device);
	virtual ~V4l2Access();

	int GetFd() {
		return device->GetFd();
	}

	unsigned int GetBufferSize() {
		return device->GetBufferSize();
	}

	unsigned int GetFormat() {
		return device->GetFormat();
	}

	unsigned int GetWidth() {
		return device->GetWidth();
	}

	unsigned int GetHeight() {
		return device->GetHeight();
	}

	void QueryFormat() {
		device->QueryFormat();
	}

	int SetFormat(unsigned int format, unsigned int width, unsigned int height) {
		return device->SetFormat(format, width, height);
	}

	int SetFps(int fps) {
		return device->SetFps(fps);
	}

	int IsReady() {
		return device->IsReady();
	}

	int Start() {
		return device->Start();
	}

	int Stop() {
		return device->Stop();
	}

private:
	V4l2Access(const V4l2Access &);
	V4l2Access & operator = (const V4l2Access &);

protected:
	V4l2Device *device;
};

#endif
