#include <string.h>

#include <linux/videodev2.h>

#include "v4l2/logger.h"
#include "v4l2/V4l2Output.h"
#include "v4l2/V4l2MmapDevice.h"
#include "v4l2/V4l2ReadWriteDevice.h"

V4l2Output *V4l2Output::Create(const V4L2DeviceParameters &params) {
	V4l2Output *output = NULL;
	V4l2Device *device = NULL;
	int caps = V4L2_CAP_VIDEO_OUTPUT;

	switch (params.iotype) {
		case IOTYPE_MMAP:
			device = new V4l2MmapDevice(params, V4L2_BUF_TYPE_VIDEO_OUTPUT);
			caps |= V4L2_CAP_STREAMING;
			break;
		case IOTYPE_READWRITE:
			device = new V4l2ReadWriteDevice(params, V4L2_BUF_TYPE_VIDEO_OUTPUT);
			caps |= V4L2_CAP_READWRITE;
			break;
	}

	if (device && !device->Init(caps)) {
		delete device;
		device = NULL;
	}

	if (device)
		output = new V4l2Output(device);

	return output;
}

V4l2Output::V4l2Output(V4l2Device *device) : V4l2Access(device) {
}

V4l2Output::~V4l2Output() {
}

bool V4l2Output::IsWritable(timeval *tv) {
	int fd = device->GetFd();
	fd_set fdset;
	FD_ZERO(&fdset);
	FD_SET(fd, &fdset);

	return select(fd + 1, NULL, &fdset, NULL, tv) == 1;
}

size_t V4l2Output::Write(char *buffer, size_t bufferSize) {
	return device->WriteInternal(buffer, bufferSize);
}

bool V4l2Output::StartPartialWrite() {
	return device->StartPartialWrite();
}

size_t V4l2Output::WritePartial(char *buffer, size_t bufferSize) {
	return device->WritePartialInternal(buffer, bufferSize);
}

bool V4l2Output::EndPartialWrite() {
	return device->EndPartialWrite();
}
