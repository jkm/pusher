#ifndef V4L2_OUTPUT
#define V4L2_OUTPUT

#include "V4l2Access.h"

class V4l2Output : public V4l2Access {
protected:
	V4l2Output(V4l2Device *device);

public:
	static V4l2Output *Create(const V4L2DeviceParameters &params);
	virtual ~V4l2Output();

	bool IsWritable(timeval *tv);
	size_t Write(char *buffer, size_t bufferSize);
	bool StartPartialWrite();
	size_t WritePartial(char *buffer, size_t bufferSize);
	bool EndPartialWrite();
};

#endif
