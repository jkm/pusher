#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <linux/videodev2.h>

#include "v4l2/logger.h"
#include "v4l2/V4l2MmapDevice.h"

V4l2MmapDevice::V4l2MmapDevice(const V4L2DeviceParameters & params, v4l2_buf_type deviceType) : V4l2Device(params, deviceType), bufferIndex(0) {
	memset(&nBuffer, 0, sizeof(nBuffer));
}

bool V4l2MmapDevice::Init(unsigned int caps) {
	bool ret = V4l2Device::Init(caps);
	if (ret)
		ret = this->Start();

	return ret;
}

V4l2MmapDevice::~V4l2MmapDevice() {
	this->Stop();
}

bool V4l2MmapDevice::Start() {
	LOG(NOTICE) << "Device " << params.devName;

	bool success = true;
	struct v4l2_requestbuffers req;
	memset(&req, 0, sizeof(req));
	req.count	= V4L2MMAP_NBBUFFER;
	req.type	= deviceType;
	req.memory	= V4L2_MEMORY_MMAP;

	if (-1 == ioctl(fd, VIDIOC_REQBUFS, &req)) {
		if (EINVAL == errno) {
			LOG(ERROR) << "Device " << params.devName << " does not support memory mapping";
			success = false;
		} else {
			perror("VIDIOC_REQBUFS");
			success = false;
		}
	} else {
		LOG(NOTICE) << "Device " << params.devName << " nb buffer:" << req.count;

		memset(&nBuffer, 0, sizeof(nBuffer));
		for (bufferIndex = 0; bufferIndex < req.count; ++bufferIndex) {
			struct v4l2_buffer buf;
			memset(&buf, 0, sizeof(buf));
			buf.type	= deviceType;
			buf.memory	= V4L2_MEMORY_MMAP;
			buf.index	= bufferIndex;

			if (-1 == ioctl(fd, VIDIOC_QUERYBUF, &buf)) {
				perror("VIDIOC_QUERYBUF");
				success = false;
			} else {
				LOG(INFO) << "Device " << params.devName << " buffer idx:" << bufferIndex << " size:" << buf.length << " offset:" << buf.m.offset;
				nBuffer[bufferIndex].length = buf.length;

				if (!nBuffer[bufferIndex].length)
					nBuffer[bufferIndex].length = buf.bytesused;

				nBuffer[bufferIndex].start = mmap(NULL, nBuffer[bufferIndex].length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buf.m.offset);

				if (MAP_FAILED == nBuffer[bufferIndex].start) {
					perror("mmap");
					success = false;
				}
			}
		}

		for (unsigned int i = 0; i < bufferIndex; ++i) {
			struct v4l2_buffer buf;
			memset(&buf, 0, sizeof(buf));
			buf.type	= deviceType;
			buf.memory	= V4L2_MEMORY_MMAP;
			buf.index	= i;

			if (-1 == ioctl(fd, VIDIOC_QBUF, &buf)) {
				perror("VIDIOC_QBUF");
				success = false;
			}
		}

		int type = deviceType;
		if (-1 == ioctl(fd, VIDIOC_STREAMON, &type)) {
			perror("VIDIOC_STREAMON");
			success = false;
		}
	}

	return success;
}

bool V4l2MmapDevice::Stop() {
	LOG(NOTICE) << "Device " << params.devName;

	bool success = true;

	int type = deviceType;
	if (-1 == ioctl(fd, VIDIOC_STREAMOFF, &type)) {
		perror("VIDIOC_STREAMOFF");
		success = false;
	}

	for (unsigned int i = 0; i < bufferIndex; ++i) {
		if (-1 == munmap(nBuffer[i].start, nBuffer[i].length)) {
			perror("munmap");
			success = false;
		}
	}

	struct v4l2_requestbuffers req;
	memset(&req, 0, sizeof(req));
	req.count	= 0;
	req.type	= deviceType;
	req.memory	= V4L2_MEMORY_MMAP;

	if (-1 == ioctl(fd, VIDIOC_REQBUFS, &req)) {
		perror("VIDIOC_REQBUFS");
		success = false;
	}

	bufferIndex = 0;

	return success;
}

size_t V4l2MmapDevice::ReadInternal(char *buffer, size_t bufferSize) {
	size_t size = 0;

	if (bufferIndex > 0) {
		struct v4l2_buffer buf;
		memset(&buf, 0, sizeof(buf));
		buf.type	= deviceType;
		buf.memory	= V4L2_MEMORY_MMAP;

		if (-1 == ioctl(fd, VIDIOC_DQBUF, &buf)) {
			perror("VIDIOC_DQBUF");
			size = -1;
		} else if (buf.index < bufferIndex) {
			size = buf.bytesused;
			if (size > bufferSize) {
				size = bufferSize;
				LOG(WARN) << "Device " << params.devName << " buffer truncated available:" << bufferSize << " needed:" << buf.bytesused;
			}

			memcpy(buffer, nBuffer[buf.index].start, size);

			if (-1 == ioctl(fd, VIDIOC_QBUF, &buf)) {
				perror("VIDIOC_QBUF");
				size = -1;
			}
		}
	}

	return size;
}

size_t V4l2MmapDevice::WriteInternal(char *buffer, size_t bufferSize) {
	size_t size = 0;

	if (bufferIndex > 0) {
		struct v4l2_buffer buf;	
		memset(&buf, 0, sizeof(buf));
		buf.type = deviceType;
		buf.memory = V4L2_MEMORY_MMAP;

		if (-1 == ioctl(fd, VIDIOC_DQBUF, &buf)) {
			perror("VIDIOC_DQBUF");
			size = -1;
		} else if (buf.index < bufferIndex) {
			size = bufferSize;
			if (size > buf.length) {
				LOG(WARN) << "Device " << params.devName << " buffer truncated available:" << buf.length << " needed:" << size;
				size = buf.length;
			}

			memcpy(nBuffer[buf.index].start, buffer, size);
			buf.bytesused = size;

			if (-1 == ioctl(fd, VIDIOC_QBUF, &buf)) {
				perror("VIDIOC_QBUF");
				size = -1;
			}
		}
	}

	return size;
}

bool V4l2MmapDevice::StartPartialWrite() {
	if (bufferIndex <= 0)
		return false;

	if (partialWriteInProgress)
		return false;

	memset(&partialWriteBuf, 0, sizeof(partialWriteBuf));
	partialWriteBuf.type = deviceType;
	partialWriteBuf.memory = V4L2_MEMORY_MMAP;

	if (-1 == ioctl(fd, VIDIOC_DQBUF, &partialWriteBuf)) {
		perror("VIDIOC_DQBUF");
		return false;
	}

	partialWriteBuf.bytesused = 0;
	partialWriteInProgress = true;

	return true;
}

size_t V4l2MmapDevice::WritePartialInternal(char* buffer, size_t bufferSize) {
	size_t new_size = 0;
	size_t size = 0;

	if ((bufferIndex > 0) && partialWriteInProgress) {
		if (partialWriteBuf.index < bufferIndex) {
			new_size = partialWriteBuf.bytesused + bufferSize;
			if (new_size > partialWriteBuf.length) {
				LOG(WARN) << "Device " << params.devName << " buffer truncated available:" << partialWriteBuf.length << " needed:" << new_size;
				new_size = partialWriteBuf.length;
			}

			size = new_size - partialWriteBuf.bytesused;
			memcpy(&((char *)nBuffer[partialWriteBuf.index].start)[partialWriteBuf.bytesused], buffer, size);

			partialWriteBuf.bytesused += size;
		}
	}

	return size;
}

bool V4l2MmapDevice::EndPartialWrite() {
	if (!partialWriteInProgress)
		return false;

	if (bufferIndex <= 0) {
		partialWriteInProgress = false;
		return true;
	}

	if (-1 == ioctl(fd, VIDIOC_QBUF, &partialWriteBuf)) {
		perror("VIDIOC_QBUF");
		partialWriteInProgress = false;
		return true;
	}

	partialWriteInProgress = false;

	return true;
}
