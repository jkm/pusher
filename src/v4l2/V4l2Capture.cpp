#include <linux/videodev2.h>

#include "v4l2/V4l2Capture.h"
#include "v4l2/V4l2MmapDevice.h"
#include "v4l2/V4l2ReadWriteDevice.h"

V4l2Capture *V4l2Capture::Create(const V4L2DeviceParameters &params) {
	V4l2Capture *capture = NULL;
	V4l2Device *device = NULL;

	int caps = V4L2_CAP_VIDEO_CAPTURE;

	switch (params.iotype) {
		case IOTYPE_MMAP:
			device = new V4l2MmapDevice(params, V4L2_BUF_TYPE_VIDEO_CAPTURE);
			caps |= V4L2_CAP_STREAMING;
			break;
		case IOTYPE_READWRITE:
			device = new V4l2ReadWriteDevice(params, V4L2_BUF_TYPE_VIDEO_CAPTURE);
			caps |= V4L2_CAP_READWRITE;
			break;
	}

	if (device && !device->Init(caps)) {
		delete device;
		device = NULL;
	}

	if (device)
		capture = new V4l2Capture(device);

	return capture;
}

V4l2Capture::V4l2Capture(V4l2Device *device) : V4l2Access(device) {
}

V4l2Capture::~V4l2Capture() {
}

bool V4l2Capture::IsReadable() {
	struct timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	int fd = device->GetFd();
	fd_set fdset;
	FD_ZERO(&fdset);
	FD_SET(fd, &fdset);

	return select(fd + 1, &fdset, NULL, NULL, &timeout) == 1;
}

size_t V4l2Capture::Read(char *buffer, size_t bufferSize) {
	return device->ReadInternal(buffer, bufferSize);
}

void V4l2Capture::Start() {
	device->Start();
}

void V4l2Capture::Stop() {
	device->Stop();
}
