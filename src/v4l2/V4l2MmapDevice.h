#ifndef V4L2_MMAP_DEVICE
#define V4L2_MMAP_DEVICE

#include "V4l2Device.h"

#define V4L2MMAP_NBBUFFER 8

class V4l2MmapDevice : public V4l2Device {
protected:
	size_t WriteInternal(char *buffer, size_t bufferSize);
	bool StartPartialWrite();
	size_t WritePartialInternal(char *, size_t);
	bool EndPartialWrite();
	size_t ReadInternal(char *buffer, size_t bufferSize);

public:
	V4l2MmapDevice(const V4L2DeviceParameters &params, v4l2_buf_type deviceType);
	virtual ~V4l2MmapDevice();

	virtual bool isReady() {
		return (fd != -1) && (bufferIndex != 0);
	}

	virtual bool Init(unsigned int caps);
	virtual bool Start();
	virtual bool Stop();

protected:
	unsigned int bufferIndex;

	struct buffer {
		void *start;
		size_t length;
	};
	buffer nBuffer[V4L2MMAP_NBBUFFER];
};

#endif
