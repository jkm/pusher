#ifndef LOGGER_H
#define LOGGER_H

#include <unistd.h>
#include <iostream>

typedef enum {
	EMERG  = 0,
	FATAL  = 0,
	ALERT  = 100,
	CRIT   = 200,
	ERROR  = 300,
	WARN   = 400,
	NOTICE = 500,
	INFO   = 600,
	DEBUG  = 700,
	NOTSET = 800
} PriorityLevel;

extern int LogLevel;
#define LOG(__level) if (__level<=LogLevel) std::cout << "\n[" << #__level << "] " << __FILE__ << ":" << __LINE__ << "\n\t"

inline void initLogger(int verbose) {
        switch (verbose) {
		case 1:
			LogLevel = INFO;
			break;
		case 2:
			LogLevel = DEBUG;
			break;
		default:
			LogLevel = NOTICE;
			break;
        }

	std::cout << "log level:" << LogLevel << std::endl;
}

#endif
