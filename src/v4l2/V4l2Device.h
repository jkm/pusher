#ifndef V4L2_DEVICE
#define V4L2_DEVICE

#include <string>
#include <list>
#include <linux/videodev2.h>
#include <fcntl.h>

#ifndef V4L2_PIX_FMT_VP8
#define V4L2_PIX_FMT_VP8	v4l2_fourcc('V', 'P', '8', '0')
#endif

#ifndef V4L2_PIX_FMT_VP9
#define V4L2_PIX_FMT_VP9	v4l2_fourcc('V', 'P', '9', '0')
#endif

#ifndef V4L2_PIX_FMT_HEVC
#define V4L2_PIX_FMT_HEVC	v4l2_fourcc('H', 'E', 'V', 'C')
#endif

enum V4l2IoType {
	IOTYPE_READWRITE,
	IOTYPE_MMAP
};

struct V4L2DeviceParameters {
	V4L2DeviceParameters(const char *devName,
			     const std::list<unsigned int> &formatList,
			     unsigned int width,
			     unsigned int height,
			     int fps,
			     V4l2IoType ioType = IOTYPE_MMAP,
			     int verbose = 0,
			     int openFlags = O_RDWR | O_NONBLOCK
		) : devName(devName), formatList(formatList), width(width), height(height), fps(fps), iotype(ioType), verbose(verbose), openFlags(openFlags) {
	}

	V4L2DeviceParameters(const char *devName,
			     unsigned int format,
			     unsigned int width,
			     unsigned int height,
			     int fps,
			     V4l2IoType ioType = IOTYPE_MMAP,
			     int verbose = 0,
			     int openFlags = O_RDWR | O_NONBLOCK
		) : devName(devName), width(width), height(height), fps(fps), iotype(ioType), verbose(verbose), openFlags(openFlags) {
		if (format)
			formatList.push_back(format);
	}

	std::string devName;
	std::list<unsigned int> formatList;
	unsigned int width;
	unsigned int height;
	int fps;
	V4l2IoType iotype;
	int verbose;
	int openFlags;
};

class V4l2Device {
friend class V4l2Capture;
friend class V4l2Output;

protected:
	void Close();

	int InitDevice(const char *devName, unsigned int caps);
	int CheckCaps(int fd, unsigned int caps);
	int SetFormat(int fd);
	int SetFormat(int fd, unsigned int format, unsigned int width, unsigned int height);
	int SetFps(int fd, int fps);

	virtual bool Init(unsigned int caps);

	virtual size_t WriteInternal(char *, size_t) {
		return -1;
	}

	virtual bool StartPartialWrite() {
		return false;
	}

	virtual size_t WritePartialInternal(char *, size_t) {
		return -1;
	}

	virtual bool EndPartialWrite() {
		return false;
	}

	virtual size_t ReadInternal(char *, size_t) {
		return -1;
	}

public:
	V4l2Device(const V4L2DeviceParameters &params, v4l2_buf_type deviceType);
	virtual ~V4l2Device();

	virtual bool IsReady() {
		return (fd != -1);
	}

	virtual bool Start() {
		return true;
	}

	virtual bool Stop() {
		return true;
	}

	unsigned int GetBufferSize() {
		return bufferSize;
	}

	unsigned int GetFormat() {
		return format;
	}

	unsigned int GetWidth() {
		return width;
	}

	unsigned int GetHeight() {
		return height;
	}

	int GetFd() {
		return fd;
	}

	void QueryFormat();

	int SetFormat(unsigned int format, unsigned int width, unsigned int height) {
		return this->SetFormat(fd, format, width, height);
	}

	int SetFps(int fps) {
		return this->SetFps(fd, fps);
	}

	static std::string fourcc(unsigned int format);
	static unsigned int fourcc(const char *format);

protected:
	V4L2DeviceParameters params;
	int fd;
	v4l2_buf_type deviceType;

	unsigned int bufferSize;
	unsigned int format;
	unsigned int width;
	unsigned int height;

	struct v4l2_buffer partialWriteBuf;
	bool partialWriteInProgress;
};

#endif
