#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

#include <linux/videodev2.h>

#include "v4l2/logger.h"
#include "v4l2/V4l2Device.h"

std::string V4l2Device::fourcc(unsigned int format) {
	char formatArray[] = {
		(char)(format & 0xff),
		(char)((format >> 8) & 0xff),
		(char)((format >> 16) & 0xff),
		(char)((format >> 24) & 0xff),
		0
	};

	return std::string(formatArray, strlen(formatArray));
}

unsigned int V4l2Device::fourcc(const char *format) {
	char fourcc[4];
	memset(&fourcc, 0, sizeof(fourcc));

	if (format != NULL)
		strncpy(fourcc, format, 4);

	return v4l2_fourcc(fourcc[0], fourcc[1], fourcc[2], fourcc[3]);
}

V4l2Device::V4l2Device(const V4L2DeviceParameters &params, v4l2_buf_type deviceType) : params(params), fd(-1), deviceType(deviceType), bufferSize(0), format(0) {
}

V4l2Device::~V4l2Device() {
	this->Close();
}

void V4l2Device::Close() {
	if (fd != -1)
		::close(fd);

	fd = -1;
}

void V4l2Device::QueryFormat() {
	struct v4l2_format fmt;
	memset(&fmt, 0, sizeof(fmt));
	fmt.type = deviceType;

	if (ioctl(fd, VIDIOC_G_FMT, &fmt) == 0) {
		format		= fmt.fmt.pix.pixelformat;
		width		= fmt.fmt.pix.width;
		height		= fmt.fmt.pix.height;
		bufferSize	= fmt.fmt.pix.sizeimage;

		LOG(DEBUG) << params.devName << ":" << fourcc(format) << " size:" << width << "x" << height << " bufferSize:" << bufferSize;
	}
}

bool V4l2Device::Init(unsigned int caps) {
	struct stat sb;

	if ((stat(params.devName.c_str(), &sb) == 0) && ((sb.st_mode & S_IFMT) == S_IFCHR)) {
		if (InitDevice(params.devName.c_str(), caps) == -1)
			LOG(ERROR) << "Cannot init device:" << params.devName;
	} else {
		fd = open(params.devName.c_str(), O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
	}

	return (fd != -1);
}

int V4l2Device::InitDevice(const char *devName, unsigned int caps) {
	fd = open(devName, params.openFlags);

	if (fd < 0) {
		LOG(ERROR) << "Cannot open device:" << params.devName << " " << strerror(errno);
		this->Close();
		return -1;
	}

	if (CheckCaps(fd, caps) != 0) {
		this->Close();
		return -1;
	}

	if (SetFormat(fd) != 0) {
		this->Close();
		return -1;
	}

	if (SetFps(fd, params.fps) != 0) {
		this->Close();
		return -1;
	}

	return fd;
}

int V4l2Device::CheckCaps(int fd, unsigned int caps) {
	struct v4l2_capability cap;
	memset(&cap, 0, sizeof(cap));

	if (-1 == ioctl(fd, VIDIOC_QUERYCAP, &cap)) {
		LOG(ERROR) << "Cannot get capabilities for device:" << params.devName << " " << strerror(errno);
		return -1;
	}

	LOG(NOTICE) << "driver:" << cap.driver << " capabilities:" << std::hex << cap.capabilities << " mandatory:" << caps << std::dec;

	if ((cap.capabilities & V4L2_CAP_VIDEO_OUTPUT))
		LOG(NOTICE) << params.devName << " support output";

	if ((cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
		LOG(NOTICE) << params.devName << " support capture";

	if ((cap.capabilities & V4L2_CAP_READWRITE))
		LOG(NOTICE) << params.devName << " support read/write";

	if ((cap.capabilities & V4L2_CAP_STREAMING))
		LOG(NOTICE) << params.devName << " support streaming";

	if ((cap.capabilities & V4L2_CAP_TIMEPERFRAME))
		LOG(NOTICE) << params.devName << " support timeperframe";

	if ((cap.capabilities & caps) != caps) {
		LOG(ERROR) << "Mandatory capability not available for device:" << params.devName;
		return -1;
	}

	return 0;
}

int V4l2Device::SetFormat(int fd) {
	this->QueryFormat();

	unsigned int width = width;
	unsigned int height = height;

	if (params.width != 0)
		width = params.width;

	if (params.height != 0)
		height = params.height;

	if ((params.formatList.size() == 0) && (format != 0))
		params.formatList.push_back(format);

	std::list<unsigned int>::iterator it;
	for (it = params.formatList.begin(); it != params.formatList.end(); ++it) {
		unsigned int format = *it;
		if (this->SetFormat(fd, format, width, height) == 0) {
			this->QueryFormat();
			return 0;
		}
	}

	return -1;
}

int V4l2Device::SetFormat(int fd, unsigned int format, unsigned int width, unsigned int height) {
	struct v4l2_format fmt;

	memset(&fmt, 0, sizeof(fmt));
	fmt.type = deviceType;

	if (ioctl(fd,VIDIOC_G_FMT, &fmt) == -1) {
		LOG(ERROR) << params.devName << ": Cannot get format " << strerror(errno);
		return -1;
	}

	if (width != 0)
		fmt.fmt.pix.width = width;

	if (height != 0)
		fmt.fmt.pix.height = height;

	if (format != 0)
		fmt.fmt.pix.pixelformat = format;

	if (ioctl(fd, VIDIOC_S_FMT, &fmt) == -1) {
		LOG(ERROR) << params.devName << ": Cannot set format:" << fourcc(format) << " " << strerror(errno);
		return -1;
	}

	if (fmt.fmt.pix.pixelformat != format) {
		LOG(ERROR) << params.devName << ": Cannot set pixelformat to:" << fourcc(format) << " format is:" << fourcc(fmt.fmt.pix.pixelformat);
		return -1;
	}

	if ((fmt.fmt.pix.width != width) || (fmt.fmt.pix.height != height))
		LOG(WARN) << params.devName << ": Cannot set size to:" << width << "x" << height << " size is:" << fmt.fmt.pix.width << "x" << fmt.fmt.pix.height;

	format		= fmt.fmt.pix.pixelformat;
	width		= fmt.fmt.pix.width;
	height		= fmt.fmt.pix.height;
	bufferSize	= fmt.fmt.pix.sizeimage;

	LOG(NOTICE) << params.devName << ":" << fourcc(format) << " size:" << width << "x" << height << " bufferSize:" << bufferSize;

	return 0;
}

int V4l2Device::SetFps(int fd, int fps) {
	if (fps != 0) {
		struct v4l2_streamparm param;
		memset(&param, 0, sizeof(param));

		param.type = deviceType;
		param.parm.capture.timeperframe.numerator = 1;
		param.parm.capture.timeperframe.denominator = fps;

		if (ioctl(fd, VIDIOC_S_PARM, &param) == -1)
			LOG(WARN) << "Cannot set param for device:" << params.devName << " " << strerror(errno);

		LOG(NOTICE) << "fps:" << param.parm.capture.timeperframe.numerator << "/" << param.parm.capture.timeperframe.denominator;
		LOG(NOTICE) << "nbBuffer:" << param.parm.capture.readbuffers;
	}

	return 0;
}
