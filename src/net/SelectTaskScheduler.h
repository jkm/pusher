#ifndef XOP_SELECT_TASK_SCHEDULER_H
#define XOP_SELECT_TASK_SCHEDULER_H

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <mutex>
#include <unordered_map>

#include "Socket.h"
#include "TaskScheduler.h"

namespace xop {

class SelectTaskScheduler : public TaskScheduler {
public:
	SelectTaskScheduler(int id = 0);
	virtual ~SelectTaskScheduler();

	void UpdateChannel(ChannelPtr channel);
	void RemoveChannel(ChannelPtr &channel);
	bool HandleEvent(int timeout);

private:
	fd_set fdReadBackup;
	fd_set fdWriteBackup;
	fd_set fdExpBackup;
	SOCKET maxfd = 0;

	bool isFdReadReset = false;
	bool isFdWriteReset = false;
	bool isFdExpReset = false;

	std::mutex mutex;
	std::unordered_map<SOCKET, ChannelPtr> channels;
};

}

#endif
