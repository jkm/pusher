#include "net/TcpConnection.h"
#include "net/SocketUtil.h"

using namespace xop;

TcpConnection::TcpConnection(TaskScheduler *scheduler, SOCKET sockfd) : scheduler(scheduler), readBuffer(new BufferReader), writeBuffer(new BufferWriter(500)), channel(new Channel(sockfd)) {
	isClosed = false;

	channel->SetReadCallback([this]() {
		this->HandleRead();
	});

	channel->SetWriteCallback([this]() {
		this->HandleWrite();
	});

	channel->SetCloseCallback([this]() {
		this->HandleClose();
	});

	channel->SetErrorCallback([this]() {
		this->HandleError();
	});

	SocketUtil::SetNonBlock(sockfd);
	SocketUtil::SetSendBufSize(sockfd, 100 * 1024);
	SocketUtil::SetKeepAlive(sockfd);

	channel->EnableReading();
	scheduler->UpdateChannel(channel);
}

TcpConnection::~TcpConnection() {
	SOCKET fd = channel->GetSocket();
	if (fd > 0)
		SocketUtil::Close(fd);
}

void TcpConnection::Send(std::shared_ptr<char> data, uint32_t size) {
	if (!isClosed) {
		mutex.lock();
		writeBuffer->Append(data, size);
		mutex.unlock();

		this->HandleWrite();
	}
}

void TcpConnection::Send(const char *data, uint32_t size) {
	if (!isClosed) {
		mutex.lock();
		writeBuffer->Append(data, size);
		mutex.unlock();

		this->HandleWrite();
	}
}

void TcpConnection::Disconnect() {
	std::lock_guard<std::mutex> lock(mutex);
	auto conn = shared_from_this();
	scheduler->AddTriggerEvent([conn]() {
		conn->Close();
	});
}

void TcpConnection::HandleRead() {
	{
		std::lock_guard<std::mutex> lock(mutex);

		if (isClosed)
			return;

		int ret = readBuffer->Read(channel->GetSocket());
		if (ret <= 0) {
			this->Close();
			return;
		}
	}

	if (readCb) {
		bool ret = readCb(shared_from_this(), *readBuffer);
		if (false == ret) {
			std::lock_guard<std::mutex> lock(mutex);
			this->Close();
		}
	}
}

void TcpConnection::HandleWrite() {
	if (isClosed)
		return;

	// std::lock_guard<std::mutex> lock(mutex);
	if (!mutex.try_lock())
		return;

	int ret = 0;
	bool empty = false;
	do {
		ret = writeBuffer->Send(channel->GetSocket());
		if (ret < 0) {
			this->Close();
			mutex.unlock();
			return;
		}
		empty = writeBuffer->IsEmpty();
	} while (0);

	if (empty) {
		if (channel->IsWriting()) {
			channel->DisableWriting();
			scheduler->UpdateChannel(channel);
		}
	} else if (!channel->IsWriting()) {
		channel->EnableWriting();
		scheduler->UpdateChannel(channel);
	}

	mutex.unlock();
}

void TcpConnection::Close() {
	if (!isClosed) {
		isClosed = true;
		scheduler->RemoveChannel(channel);

		if (closeCb)
			closeCb(shared_from_this());

		if (disconnectCb)
			disconnectCb(shared_from_this());
	}
}

void TcpConnection::HandleClose() {
	std::lock_guard<std::mutex> lock(mutex);
	this->Close();
}

void TcpConnection::HandleError() {
	std::lock_guard<std::mutex> lock(mutex);
	this->Close();
}
