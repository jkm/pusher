#include "net/EventLoop.h"

using namespace xop;

EventLoop::EventLoop(uint32_t nthreads) : index(1) {
	if (nthreads > 0)
		num_threads = nthreads;

	this->Loop();
}

EventLoop::~EventLoop() {
	this->Quit();
}

std::shared_ptr<TaskScheduler> EventLoop::GetTaskScheduler() {
	std::lock_guard<std::mutex> locker(mutex);

	if (schedulers.size() == 1) {
		return schedulers.at(0);
	} else {
		auto scheduler = schedulers.at(index);
		index++;

		if (index >= schedulers.size())
			index = 1;

		return scheduler;
	}

	return nullptr;
}

void EventLoop::Loop() {
	std::lock_guard<std::mutex> locker(mutex);

	if (!schedulers.empty())
		return;

	for (uint32_t n = 0; n < num_threads; n++) {
		std::shared_ptr<TaskScheduler> schedulerPtr(new EpollTaskScheduler(n));
		schedulers.push_back(schedulerPtr);
		std::shared_ptr<std::thread> thread(new std::thread(&TaskScheduler::Start, schedulerPtr.get()));
		thread->native_handle();
		threads.push_back(thread);
	}
}

void EventLoop::Quit() {
	std::lock_guard<std::mutex> locker(mutex);

	for (auto iter : schedulers)
		iter->Stop();

	for (auto iter : threads)
		iter->join();

	schedulers.clear();
	threads.clear();
}

void EventLoop::UpdateChannel(ChannelPtr channel) {
	std::lock_guard<std::mutex> locker(mutex);

	if (schedulers.size() > 0)
		schedulers[0]->UpdateChannel(channel);
}

void EventLoop::RemoveChannel(ChannelPtr& channel) {
	std::lock_guard<std::mutex> locker(mutex);

	if (schedulers.size() > 0)
		schedulers[0]->RemoveChannel(channel);
}

TimerId EventLoop::AddTimer(TimerEvent timerEvent, uint32_t msec) {
	std::lock_guard<std::mutex> locker(mutex);

	if (schedulers.size() > 0)
		return schedulers[0]->AddTimer(timerEvent, msec);

	return 0;
}

void EventLoop::RemoveTimer(TimerId timerId) {
	std::lock_guard<std::mutex> locker(mutex);

	if (schedulers.size() > 0)
		schedulers[0]->RemoveTimer(timerId);
}

bool EventLoop::AddTriggerEvent(TriggerEvent callback) {   
	std::lock_guard<std::mutex> locker(mutex);

	if (schedulers.size() > 0)
		return schedulers[0]->AddTriggerEvent(callback);

	return false;
}
