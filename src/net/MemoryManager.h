#ifndef XOP_MEMMORY_MANAGER_H
#define XOP_MEMMORY_MANAGER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <mutex>

namespace xop {

void *Alloc(uint32_t size);
void Free(void *ptr);

class MemoryPool;

struct MemoryBlock {
	uint32_t blockId = 0;
	MemoryPool *pool = nullptr;
	MemoryBlock *next = nullptr;
};

class MemoryPool {
public:
	MemoryPool();
	virtual ~MemoryPool();

	void Init(uint32_t size, uint32_t n);
	void *Alloc(uint32_t size);
	void Free(void *ptr);

	size_t BlockSize() const {
		return blockSize;
	}

private:
	char *memory = nullptr;
	uint32_t blockSize = 0;
	uint32_t numBlocks = 0;
	MemoryBlock *head = nullptr;
	std::mutex mutex;
};

class MemoryManager {
public:
	static MemoryManager &Instance();
	~MemoryManager();

	void *Alloc(uint32_t size);
	void Free(void *ptr);

private:
	MemoryManager();

	static const int kMaxMemoryPool = 3;
	MemoryPool pools[kMaxMemoryPool];
};

}
#endif
