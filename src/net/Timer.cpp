#include <iostream>

#include "net/Timer.h"

using namespace xop;

TimerId TimerQueue::AddTimer(const TimerEvent &event, uint32_t ms) {
	std::lock_guard<std::mutex> locker(mutex);

	int64_t timeout = GetTimeNow();
	TimerId timerId = ++lastTimerId;

	auto timer = std::make_shared<Timer>(event, ms);
	timer->SetNextTimeout(timeout);
	timers.emplace(timerId, timer);
	events.emplace(std::pair<int64_t, TimerId>(timeout + ms, timerId), std::move(timer));

	return timerId;
}

void TimerQueue::RemoveTimer(TimerId timerId) {
	std::lock_guard<std::mutex> locker(mutex);

	auto iter = timers.find(timerId);
	if (iter != timers.end()) {
		int64_t timeout = iter->second->GetNextTimeout();
		events.erase(std::pair<int64_t, TimerId>(timeout, timerId));
		timers.erase(timerId);
	}
}

int64_t TimerQueue::GetTimeNow() {
	auto timePoint = std::chrono::steady_clock::now();
	return std::chrono::duration_cast<std::chrono::milliseconds>(timePoint.time_since_epoch()).count();
}

int64_t TimerQueue::GetTimeRemaining() {
	std::lock_guard<std::mutex> locker(mutex);

	if (timers.empty())
		return -1;

	int64_t msec = events.begin()->first.first - GetTimeNow();
	if (msec < 0)
		msec = 0;

	return msec;
}

void TimerQueue::HandleTimerEvent() {
	if (!timers.empty()) {
		std::lock_guard<std::mutex> locker(mutex);

		int64_t timePoint = GetTimeNow();

		while (!timers.empty() && events.begin()->first.first <= timePoint) {
			TimerId timerId = events.begin()->first.second;
			bool flag = events.begin()->second->event_callback();
			if (flag == true) {
				events.begin()->second->SetNextTimeout(timePoint);
				auto timerPtr = std::move(events.begin()->second);
				events.erase(events.begin());
				events.emplace(std::pair<int64_t, TimerId>(timerPtr->GetNextTimeout(), timerId), timerPtr);
			} else {
				events.erase(events.begin());
				timers.erase(timerId);
			}
		}
	}
}
