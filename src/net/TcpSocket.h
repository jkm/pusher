#ifndef XOP_TCP_SOCKET_H
#define XOP_TCP_SOCKET_H

#include <cstdint>
#include <string>

#include "Socket.h"

namespace xop {

class TcpSocket {
public:
	TcpSocket(SOCKET sockfd = -1);
	virtual ~TcpSocket();

	SOCKET Accept();
	SOCKET Create();
	SOCKET GetSocket() const {
		return sockfd;
	}

	bool Bind(std::string ip, uint16_t port);
	bool Listen(int backlog);
	bool Connect(std::string ip, uint16_t port, int timeout = 0);
	void Close();
	void ShutdownWrite();

private:
	SOCKET sockfd = -1;
};

}

#endif
