#include "net/BufferWriter.h"
#include "net/Socket.h"
#include "net/SocketUtil.h"

using namespace xop;

void xop::WriteUint32BE(char *p, uint32_t value) {
	p[0] = value >> 24;
	p[1] = value >> 16;
	p[2] = value >> 8;
	p[3] = value & 0xff;
}

void xop::WriteUint32LE(char *p, uint32_t value) {
	p[0] = value & 0xff;
	p[1] = value >> 8;
	p[2] = value >> 16;
	p[3] = value >> 24;
}

void xop::WriteUint24BE(char *p, uint32_t value) {
	p[0] = value >> 16;
	p[1] = value >> 8;
	p[2] = value & 0xff;
}

void xop::WriteUint24LE(char *p, uint32_t value) {
	p[0] = value & 0xff;
	p[1] = value >> 8;
	p[2] = value >> 16;
}

void xop::WriteUint16BE(char *p, uint16_t value) {
	p[0] = value >> 8;
	p[1] = value & 0xff;
}

void xop::WriteUint16LE(char *p, uint16_t value) {
	p[0] = value & 0xff;
	p[1] = value >> 8;
}

BufferWriter::BufferWriter(int capacity) : max_queue_length(capacity) {
}	

bool BufferWriter::Append(std::shared_ptr<char> data, uint32_t size, uint32_t index) {
	if (size <= index)
		return false;

	if ((int)buffer.size() >= max_queue_length)
		return false;

	Packet pkt = { data, size, index };
	buffer.emplace(std::move(pkt));

	return true;
}

bool BufferWriter::Append(const char *data, uint32_t size, uint32_t index) {
	if (size <= index)
		return false;

	if ((int)buffer.size() >= max_queue_length)
		return false;

	Packet pkt;
	pkt.data.reset(new char[size + 512]);
	memcpy(pkt.data.get(), data, size);
	pkt.size = size;
	pkt.writeIndex = index;
	buffer.emplace(std::move(pkt));

	return true;
}

int BufferWriter::Send(SOCKET sockfd, int timeout) {		
	if (timeout > 0)
		SocketUtil::SetBlock(sockfd, timeout); 

	int ret = 0;
	int count = 1;

	do {
		if (buffer.empty())
			return 0;

		count -= 1;
		Packet &pkt = buffer.front();

		ret = ::send(sockfd, pkt.data.get() + pkt.writeIndex, pkt.size - pkt.writeIndex, 0);

		if (ret > 0) {
			pkt.writeIndex += ret;
			if (pkt.size == pkt.writeIndex) {
				count += 1;
				buffer.pop();
			}
		}

		if (ret < 0 && (errno == EINTR || errno == EAGAIN))
			ret = 0;
	} while (count > 0);

	if (timeout > 0)
		SocketUtil::SetNonBlock(sockfd);

	return ret;
}
