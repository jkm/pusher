#ifndef XOP_TCPSERVER_H
#define XOP_TCPSERVER_H

#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>

#include "Socket.h"
#include "TcpConnection.h"


namespace xop {

class Acceptor;
class EventLoop;

class TcpServer {
public:	
	TcpServer(EventLoop *loop);
	virtual ~TcpServer();

	virtual bool Start(std::string ip, uint16_t port);
	virtual void Stop();

	std::string GetIPAddress() const {
		return ip;
	}

	uint16_t GetPort() const {
		return port;
	}

protected:
	virtual TcpConnection::Ptr OnConnect(SOCKET sockFd);
	virtual void AddConnection(SOCKET sockFd, TcpConnection::Ptr tcp_conn);
	virtual void RemoveConnection(SOCKET sockFd);

	EventLoop *loop;
	uint16_t port;
	std::string ip;
	std::unique_ptr<Acceptor> acceptor;
	bool isStarted;
	std::mutex mutex;
	std::unordered_map<SOCKET, TcpConnection::Ptr> connections;
};

}

#endif
