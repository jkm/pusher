#include "net/Acceptor.h"
#include "net/EventLoop.h"
#include "net/Logger.h"
#include "net/SocketUtil.h"

using namespace xop;

Acceptor::Acceptor(EventLoop *loop) : loop(loop), socket(new TcpSocket) {
}

Acceptor::~Acceptor() {
}

int Acceptor::Listen(std::string ip, uint16_t port) {
	std::lock_guard<std::mutex> locker(mutex);

	if (socket->GetSocket() > 0)
		socket->Close();

	SOCKET sockfd = socket->Create();
	channelPtr.reset(new Channel(sockfd));
	SocketUtil::SetReuseAddr(sockfd);
	SocketUtil::SetReusePort(sockfd);
	SocketUtil::SetNonBlock(sockfd);

	if (!socket->Bind(ip, port))
		return -1;

	if (!socket->Listen(1024))
		return -1;

	channelPtr->SetReadCallback([this]() {
		this->OnAccept();
	});
	channelPtr->EnableReading();
	loop->UpdateChannel(channelPtr);

	return 0;
}

void Acceptor::Close() {
	std::lock_guard<std::mutex> locker(mutex);

	if (socket->GetSocket() > 0) {
		loop->RemoveChannel(channelPtr);
		socket->Close();
	}
}

void Acceptor::OnAccept() {
	std::lock_guard<std::mutex> locker(mutex);

	SOCKET sock = socket->Accept();
	if (sock > 0) {
		if (newConnectionCb)
			newConnectionCb(sock);
		else
			SocketUtil::Close(sock);
	}
}
