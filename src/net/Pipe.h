#ifndef XOP_PIPE_H
#define XOP_PIPE_H

#include "TcpSocket.h"


namespace xop {

class Pipe {
public:
	Pipe();
	virtual ~Pipe();
	bool Create();
	int Write(void *buf, int len);
	int Read(void *buf, int len);
	void Close();

	SOCKET Read() const {
		return pipe_fd[0];
	}
	SOCKET Write() const {
		return pipe_fd[1];
	}

private:
	SOCKET pipe_fd[2];
};

}

#endif
