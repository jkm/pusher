#ifndef XOP_TIMESTAMP_H
#define XOP_TIMESTAMP_H

#include <chrono>
#include <cstdint>
#include <functional>
#include <string>
#include <thread>

namespace xop {

class Timestamp {
public:
	Timestamp() : beginTimePoint(std::chrono::high_resolution_clock::now()) { }

	void Reset() {
		beginTimePoint = std::chrono::high_resolution_clock::now();
	}

	int64_t Elapsed() {
		return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - beginTimePoint).count();
	}

	static std::string Localtime();

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> beginTimePoint;
};

}

#endif
