#ifndef XOP_CHANNEL_H
#define XOP_CHANNEL_H

#include <functional>
#include <memory>

#include "Socket.h"

namespace xop {

enum EventType {
	EVENT_NONE   = 0,
	EVENT_IN     = 1,
	EVENT_PRI    = 2,		
	EVENT_OUT    = 4,
	EVENT_ERR    = 8,
	EVENT_HUP    = 16,
	EVENT_RDHUP  = 8192
};

class Channel {
public:
	typedef std::function<void()> EventCallback;

	Channel() = delete;

	Channel(SOCKET sockfd) : sockfd(sockfd) {
	}

	virtual ~Channel() {};
    
	void SetReadCallback(const EventCallback& cb) {
		read_callback = cb;
	}

	void SetWriteCallback(const EventCallback& cb) {
		write_callback = cb;
	}

	void SetCloseCallback(const EventCallback& cb) {
		close_callback = cb;
	}

	void SetErrorCallback(const EventCallback& cb) {
		error_callback = cb;
	}

	SOCKET GetSocket() const {
		return sockfd;
	}

	int GetEvents() const {
		return events;
	}

	void SetEvents(int events) {
		this->events = events;
	}

	void EnableReading() {
		events |= EVENT_IN;
	}

	void EnableWriting() {
		events |= EVENT_OUT;
	}

	void DisableReading() {
		events &= ~EVENT_IN;
	}

	void DisableWriting() {
		events &= ~EVENT_OUT;
	}

	bool IsNoneEvent() const {
		return events == EVENT_NONE;
	}

	bool IsWriting() const {
		return (events & EVENT_OUT) != 0;
	}

	bool IsReading() const {
		return (events & EVENT_IN) != 0;
	}

	void HandleEvent(int events) {
		if (events & (EVENT_PRI | EVENT_IN))
			read_callback();

		if (events & EVENT_OUT)
			write_callback();

		if (events & EVENT_HUP) {
			close_callback();
			return;
		}

		if (events & (EVENT_ERR))
			error_callback();
	}

private:
	EventCallback read_callback  = []{};
	EventCallback write_callback = []{};
	EventCallback close_callback = []{};
	EventCallback error_callback = []{};

	SOCKET sockfd = 0;
	int events = 0;
};

typedef std::shared_ptr<Channel> ChannelPtr;

}

#endif
