#ifndef XOP_EVENT_LOOP_H
#define XOP_EVENT_LOOP_H

#include <atomic>
#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <unordered_map>

#include "net/EpollTaskScheduler.h"
#include "net/Pipe.h"
#include "net/RingBuffer.h"
#include "net/SelectTaskScheduler.h"
#include "net/Timer.h"

#define TASK_SCHEDULER_PRIORITY_LOW       0
#define TASK_SCHEDULER_PRIORITY_NORMAL    1
#define TASK_SCHEDULER_PRIORITYO_HIGH     2 
#define TASK_SCHEDULER_PRIORITY_HIGHEST   3
#define TASK_SCHEDULER_PRIORITY_REALTIME  4

namespace xop {

class EventLoop {
public:
	EventLoop(const EventLoop &) = delete;
	EventLoop &operator = (const EventLoop &) = delete;
	EventLoop(uint32_t nthreads = 1);  // std::thread::hardware_concurrency()
	virtual ~EventLoop();

	std::shared_ptr<TaskScheduler> GetTaskScheduler();

	bool AddTriggerEvent(TriggerEvent callback);
	TimerId AddTimer(TimerEvent timerEvent, uint32_t msec);
	void RemoveTimer(TimerId timerId);
	void UpdateChannel(ChannelPtr channel);
	void RemoveChannel(ChannelPtr &channel);

	void Loop();
	void Quit();

private:
	std::mutex mutex;

	uint32_t num_threads = 1;
	uint32_t index = 1;

	std::vector<std::shared_ptr<TaskScheduler>> schedulers;
	std::vector<std::shared_ptr<std::thread>> threads;
};

}

#endif
