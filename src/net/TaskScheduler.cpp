#include <signal.h>

#include "net/TaskScheduler.h"

using namespace xop;

TaskScheduler::TaskScheduler(int id) : id(id), isShutdown(false), wakeupPipe(new Pipe()), triggerEvents(new xop::RingBuffer<TriggerEvent>(kMaxTriggerEvents)) {
	static std::once_flag flag;
	std::call_once(flag, [] {});

	if (wakeupPipe->Create()) {
		wakeupChannel.reset(new Channel(wakeupPipe->Read()));
		wakeupChannel->EnableReading();
		wakeupChannel->SetReadCallback([this]() {
			this->Wake();
		});
	}
}

TaskScheduler::~TaskScheduler() {
}

void TaskScheduler::Start() {
	signal(SIGPIPE, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	signal(SIGUSR1, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGKILL, SIG_IGN);

	isShutdown = false;
	while (!isShutdown) {
		this->HandleTriggerEvent();
		this->timerQueue.HandleTimerEvent();
		int64_t timeout = this->timerQueue.GetTimeRemaining();
		this->HandleEvent((int)timeout);
	}
}

void TaskScheduler::Stop() {
	isShutdown = true;
	char event = kTriggerEvent;
	wakeupPipe->Write(&event, 1);
}

TimerId TaskScheduler::AddTimer(TimerEvent timerEvent, uint32_t msec) {
	TimerId id = timerQueue.AddTimer(timerEvent, msec);
	return id;
}

void TaskScheduler::RemoveTimer(TimerId timerId) {
	timerQueue.RemoveTimer(timerId);
}

bool TaskScheduler::AddTriggerEvent(TriggerEvent callback) {
	if (triggerEvents->Size() < kMaxTriggerEvents) {
		std::lock_guard<std::mutex> lock(mutex);
		char event = kTriggerEvent;
		triggerEvents->Push(std::move(callback));
		wakeupPipe->Write(&event, 1);

		return true;
	}

	return false;
}

void TaskScheduler::Wake() {
	char event[10] = { 0 };
	while (wakeupPipe->Read(event, 10) > 0);
}

void TaskScheduler::HandleTriggerEvent() {
	do {
		TriggerEvent callback;
		if (triggerEvents->Pop(callback))
			callback();
	} while (triggerEvents->Size() > 0);
}
