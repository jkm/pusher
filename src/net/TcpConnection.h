#ifndef XOP_TCP_CONNECTION_H
#define XOP_TCP_CONNECTION_H

#include <atomic>
#include <mutex>

#include "BufferReader.h"
#include "BufferWriter.h"
#include "Channel.h"
#include "SocketUtil.h"
#include "TaskScheduler.h"

namespace xop {

class TcpConnection : public std::enable_shared_from_this<TcpConnection> {
public:
	using Ptr = std::shared_ptr<TcpConnection>;
	using DisconnectCallback = std::function<void(std::shared_ptr<TcpConnection> conn)>;
	using CloseCallback = std::function<void(std::shared_ptr<TcpConnection> conn)>;
	using ReadCallback = std::function<bool(std::shared_ptr<TcpConnection> conn, xop::BufferReader& buffer)>;

	TcpConnection(TaskScheduler *scheduler, SOCKET sockfd);
	virtual ~TcpConnection();

	TaskScheduler *GetTaskScheduler() const {
		return scheduler;
	}

	void SetReadCallback(const ReadCallback &cb) {
		readCb = cb;
	}

	void SetCloseCallback(const CloseCallback &cb) {
		closeCb = cb;
	}

	void Send(std::shared_ptr<char> data, uint32_t size);
	void Send(const char *data, uint32_t size);

	void Disconnect();

	bool IsClosed() const {
		return isClosed;
	}

	SOCKET GetSocket() const {
		return channel->GetSocket();
	}

	uint16_t GetPort() const {
		return SocketUtil::GetPeerPort(channel->GetSocket());
	}

	std::string GetIp() const {
		return SocketUtil::GetPeerIp(channel->GetSocket());
	}

protected:
	friend class TcpServer;

	virtual void HandleRead();
	virtual void HandleWrite();
	virtual void HandleClose();
	virtual void HandleError();

	void SetDisconnectCallback(const DisconnectCallback &cb) {
		disconnectCb = cb;
	}

	TaskScheduler *scheduler;
	std::unique_ptr<xop::BufferReader> readBuffer;
	std::unique_ptr<xop::BufferWriter> writeBuffer;
	std::atomic_bool isClosed;

private:
	void Close();

	std::shared_ptr<xop::Channel> channel;
	std::mutex mutex;
	DisconnectCallback disconnectCb;
	CloseCallback closeCb;
	ReadCallback readCb;
};

}

#endif
