#include "net/Logger.h"
#include "net/Socket.h"
#include "net/SocketUtil.h"
#include "net/TcpSocket.h"

using namespace xop;

TcpSocket::TcpSocket(SOCKET sockfd) : sockfd(sockfd) {
}

TcpSocket::~TcpSocket() {
}

SOCKET TcpSocket::Create() {
	sockfd = ::socket(AF_INET, SOCK_STREAM, 0);
	return sockfd;
}

bool TcpSocket::Bind(std::string ip, uint16_t port) {
	struct sockaddr_in addr = { 0 };
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(ip.c_str()); 
	addr.sin_port = htons(port);

	if (::bind(sockfd, (struct sockaddr*)&addr, sizeof(addr)) == SOCKET_ERROR) {
		LOG_DEBUG("<socket=%d> bind <%s:%u> failed.\n", sockfd, ip.c_str(), port);
		return false;
	}

	return true;
}

bool TcpSocket::Listen(int backlog) {
	if (::listen(sockfd, backlog) == SOCKET_ERROR) {
		LOG_DEBUG("<socket=%d> listen failed.\n", sockfd);
		return false;
	}

	return true;
}

SOCKET TcpSocket::Accept() {
	struct sockaddr_in addr = { 0 };
	socklen_t addrlen = sizeof addr;

	SOCKET socket_fd = ::accept(sockfd, (struct sockaddr*)&addr, &addrlen);
	return socket_fd;
}

bool TcpSocket::Connect(std::string ip, uint16_t port, int timeout) {
	if (!SocketUtil::Connect(sockfd, ip, port, timeout)) {
		LOG_DEBUG("<socket=%d> connect failed.\n", sockfd);
		return false;
	}

	return true;
}

void TcpSocket::Close() {
	::close(sockfd);
	sockfd = 0;
}

void TcpSocket::ShutdownWrite() {
	shutdown(sockfd, SHUT_WR);
	sockfd = 0;
}
