#include <random>
#include <string>
#include <array>

#include "net/Pipe.h"
#include "net/SocketUtil.h"

using namespace xop;

Pipe::Pipe() {
}

Pipe::~Pipe() {
	Close();
}

bool Pipe::Create() {
	if (pipe2(pipe_fd, O_NONBLOCK | O_CLOEXEC) < 0)
		return false;

	return true;
}

int Pipe::Write(void *buf, int len) {
	return ::write(pipe_fd[1], buf, len);
}

int Pipe::Read(void *buf, int len) {
	return ::read(pipe_fd[0], buf, len);
}

void Pipe::Close() {
	::close(pipe_fd[0]);
	::close(pipe_fd[1]);
}
