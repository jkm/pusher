#ifndef _XOP_TIMER_H
#define _XOP_TIMER_H

#include <map>
#include <unordered_map>
#include <chrono>
#include <functional>
#include <cstdint>
#include <chrono>
#include <memory>
#include <mutex>
#include <thread>

namespace xop {
    
typedef std::function<bool(void)> TimerEvent;
typedef uint32_t TimerId;

class Timer {
public:
	Timer(const TimerEvent &event, uint32_t msec) : event_callback(event), interval(msec) {
		if (interval == 0)
			interval = 1;
	}

	static void Sleep(uint32_t msec) {
		std::this_thread::sleep_for(std::chrono::milliseconds(msec));
	}

	void SetEventCallback(const TimerEvent& event) {
		event_callback = event;
	}

	void Start(int64_t microseconds, bool repeat = false) {
		isRepeat = repeat;
		auto time_begin = std::chrono::high_resolution_clock::now();
		int64_t elapsed = 0;

		do {
			std::this_thread::sleep_for(std::chrono::microseconds(microseconds - elapsed));
			time_begin = std::chrono::high_resolution_clock::now();

			if (event_callback)
				event_callback();

			elapsed = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - time_begin).count();
			if (elapsed < 0)
				elapsed = 0;
		} while (isRepeat);
	}

	void Stop() {
		isRepeat = false;
	}	

private:
	friend class TimerQueue;

	int64_t GetNextTimeout() const {
		return nextTimeout;
	}

	void SetNextTimeout(int64_t tp) {
		nextTimeout = tp + interval;
	}

	TimerEvent event_callback = []{
		return false;
	};

	bool isRepeat = false;
	uint32_t interval = 0;
	int64_t nextTimeout = 0;
};

class TimerQueue {
public:
	TimerId AddTimer(const TimerEvent &event, uint32_t msec);
	void RemoveTimer(TimerId timerId);

	int64_t GetTimeRemaining();
	void HandleTimerEvent();

private:
	int64_t GetTimeNow();
	uint32_t lastTimerId = 0;

	std::mutex mutex;
	std::unordered_map<TimerId, std::shared_ptr<Timer>> timers;
	std::map<std::pair<int64_t, TimerId>, std::shared_ptr<Timer>> events;
};

}

#endif 
