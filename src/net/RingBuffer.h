#ifndef XOP_RING_BUFFER_H
#define XOP_RING_BUFFER_H

#include <atomic>
#include <iostream>
#include <memory>
#include <vector>

namespace xop {

template <typename T>
class RingBuffer {
public:
	RingBuffer(int capacity = 60) : capacity(capacity), num_datas(0), buffer(capacity) {
	}

	virtual ~RingBuffer() {	}

	bool Push(const T &data) {
		return pushData(std::forward<T>(data)); 
	}

	bool Push(T &&data) {
		return PushData(data); 
	}

	bool Pop(T &data) {
		if (num_datas > 0) {
			data = std::move(buffer[get_pos]);
			Add(get_pos);
			num_datas--;
			return true;
		}

		return false;
	}

	bool IsFull() const {
		return ((num_datas == capacity) ? true : false);
	}

	bool IsEmpty() const {
		return ((num_datas == 0) ? true : false);
	}

	int Size() const {
		return num_datas;
	}

private:		
	template <typename F>
	bool PushData(F &&data) {
		if (num_datas < capacity) {
			buffer[put_pos] = std::forward<F>(data);
			Add(put_pos);
			num_datas++;

			return true;
		}

		return false;
	}

	void Add(int &pos) {
		pos = (((pos + 1) == capacity) ? 0 : (pos + 1));
	}

	int capacity = 0;
	int put_pos = 0;
	int get_pos = 0;

	std::atomic_int num_datas;
	std::vector<T> buffer;
};

}

#endif
