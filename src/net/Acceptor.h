#ifndef XOP_ACCEPTOR_H
#define XOP_ACCEPTOR_H

#include <functional>
#include <memory>
#include <mutex>

#include "net/Channel.h"
#include "net/TcpSocket.h"

namespace xop {

typedef std::function<void(SOCKET)> NewConnectionCallback;

class EventLoop;

class Acceptor {
public:	
	Acceptor(EventLoop *loop);
	virtual ~Acceptor();

	void SetNewConnectionCallback(const NewConnectionCallback &cb) {
		newConnectionCb = cb;
	}

	int Listen(std::string ip, uint16_t port);
	void Close();

private:
	void OnAccept();

	EventLoop *loop = nullptr;
	std::mutex mutex;
	std::unique_ptr<TcpSocket> socket;
	ChannelPtr channelPtr;
	NewConnectionCallback newConnectionCb;
};

}

#endif 
