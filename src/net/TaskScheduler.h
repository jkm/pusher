#ifndef XOP_TASK_SCHEDULER_H
#define XOP_TASK_SCHEDULER_H

#include "Channel.h"
#include "Pipe.h"
#include "Timer.h"
#include "RingBuffer.h"

namespace xop {

typedef std::function<void(void)> TriggerEvent;

class TaskScheduler {
public:
	TaskScheduler(int id = 1);
	virtual ~TaskScheduler();

	void Start();
	void Stop();

	TimerId AddTimer(TimerEvent timerEvent, uint32_t msec);
	void RemoveTimer(TimerId timerId);
	bool AddTriggerEvent(TriggerEvent callback);

	virtual void UpdateChannel(ChannelPtr channel) { };
	virtual void RemoveChannel(ChannelPtr &channel) { };

	virtual bool HandleEvent(int timeout) {
		return false;
	};

	int GetId() const {
		return id;
	}

protected:
	void Wake();
	void HandleTriggerEvent();

	int id = 0;
	std::atomic_bool isShutdown;
	std::unique_ptr<Pipe> wakeupPipe;
	std::shared_ptr<Channel> wakeupChannel;
	std::unique_ptr<xop::RingBuffer<TriggerEvent>> triggerEvents;

	std::mutex mutex;
	TimerQueue timerQueue;

	static const char kTriggerEvent = 1;
	static const char kTimerEvent = 2;
	static const int  kMaxTriggerEvents = 50000;
};

}
#endif
