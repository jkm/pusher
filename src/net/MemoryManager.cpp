#include "net/MemoryManager.h"

using namespace xop;

void *xop::Alloc(uint32_t size) {
	return MemoryManager::Instance().Alloc(size);
}

void xop::Free(void *ptr) {
	return MemoryManager::Instance().Free(ptr);
}

MemoryPool::MemoryPool() {
}

MemoryPool::~MemoryPool() {
	if (memory)
		free(memory);
}

void MemoryPool::Init(uint32_t size, uint32_t n) {
	if (memory)
		return;

	blockSize = size;
	numBlocks = n;
	memory = (char *)malloc(numBlocks * (blockSize + sizeof(MemoryBlock)));
	head = (MemoryBlock *)memory;
	head->blockId = 1;
	head->pool = this;
	head->next = nullptr;

	MemoryBlock *current = head;
	for (uint32_t n = 1; n < numBlocks; n++) {
		MemoryBlock *next = (MemoryBlock *)(memory + (n * (blockSize + sizeof(MemoryBlock))));
		next->blockId = n + 1;
		next->pool = this;
		next->next = nullptr;

		current->next = next;
		current = next;
	}
}

void *MemoryPool::Alloc(uint32_t size) {
	std::lock_guard<std::mutex> locker(mutex);
	if (head != nullptr) {
		MemoryBlock *block = head;
		head = head->next;
		return ((char *)block + sizeof(MemoryBlock));
	}

	return nullptr;
}

void MemoryPool::Free(void *ptr) {
	MemoryBlock *block = (MemoryBlock *)((char *)ptr - sizeof(MemoryBlock));
	if (block->blockId != 0) {
		std::lock_guard<std::mutex> locker(mutex);
		block->next = head;
		head = block;
	}
}

MemoryManager::MemoryManager() {
	pools[0].Init(4096, 50);
	pools[1].Init(40960, 10);
	pools[2].Init(102400, 5);
	pools[3].Init(204800, 2);
}

MemoryManager::~MemoryManager() {
}

MemoryManager &MemoryManager::Instance() {
	static MemoryManager s_mgr;
	return s_mgr;
}

void *MemoryManager::Alloc(uint32_t size) {
	for (int n = 0; n < kMaxMemoryPool; n++) {
		if (size <= pools[n].BlockSize()) {
			void *ptr = pools[n].Alloc(size);
			if (ptr != nullptr)
				return ptr;
			else
				break;
		}
	}

	MemoryBlock *block = (MemoryBlock *)malloc(size + sizeof(MemoryBlock));
	block->blockId = 0;
	block->pool = nullptr;
	block->next = nullptr;

	return ((char *)block + sizeof(MemoryBlock));
}

void MemoryManager::Free(void *ptr) {
	MemoryBlock *block = (MemoryBlock *)((char *)ptr - sizeof(MemoryBlock));
	MemoryPool *pool = block->pool;

	if (pool != nullptr && block->blockId > 0)
		pool->Free(ptr);
	else
		::free(block);
}
