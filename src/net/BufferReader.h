#ifndef XOP_BUFFER_READER_H
#define XOP_BUFFER_READER_H

#include <algorithm>
#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include "Socket.h"

namespace xop {

uint32_t ReadUint32BE(char *data);
uint32_t ReadUint32LE(char *data);
uint32_t ReadUint24BE(char *data);
uint32_t ReadUint24LE(char *data);
uint16_t ReadUint16BE(char *data);
uint16_t ReadUint16LE(char *data);

class BufferReader {
public:	
	BufferReader(uint32_t initial_size = 2048);
	virtual ~BufferReader();

	uint32_t ReadableBytes() const {
		return (uint32_t)(writer_index - reader_index);
	}

	uint32_t WritableBytes() const {
		return (uint32_t)(buffer.size() - writer_index);
	}

	char *Peek() {
		return Begin() + reader_index;
	}

	const char *Peek() const {
		return Begin() + reader_index;
	}

	const char *FindFirstCrlf() const {
		const char* crlf = std::search(Peek(), BeginWrite(), kCRLF, kCRLF + 2);
		return crlf == BeginWrite() ? nullptr : crlf;
	}

	const char* FindLastCrlf() const {
		const char* crlf = std::find_end(Peek(), BeginWrite(), kCRLF, kCRLF + 2);
		return crlf == BeginWrite() ? nullptr : crlf;
	}

	const char* FindLastCrlfCrlf() const {
		char crlfCrlf[] = "\r\n\r\n";
		const char* crlf = std::find_end(Peek(), BeginWrite(), crlfCrlf, crlfCrlf + 4);
		return crlf == BeginWrite() ? nullptr : crlf;
	}

	void RetrieveAll() {
		writer_index = 0;
		reader_index = 0;
	}

	void Retrieve(size_t len) {
		if (len <= ReadableBytes()) {
			reader_index += len;
			if (reader_index == writer_index) {
				reader_index = 0;
				writer_index = 0;
			}
		} else {
			RetrieveAll();
		}
	}

	void RetrieveUntil(const char *end) {
		Retrieve(end - Peek());
	}

	int Read(SOCKET sockfd);
	uint32_t ReadAll(std::string &data);
	uint32_t ReadUntilCrlf(std::string &data);

	uint32_t Size() const {
		return (uint32_t)buffer.size();
	}

private:
	char *Begin() {
		return &*buffer.begin();
	}

	const char *Begin() const {
		return &*buffer.begin();
	}

	char *BeginWrite() {
		return Begin() + writer_index;
	}

	const char *BeginWrite() const {
		return Begin() + writer_index;
	}

	std::vector<char> buffer;
	size_t reader_index = 0;
	size_t writer_index = 0;

	static const char kCRLF[];
	static const uint32_t MAX_BYTES_PER_READ = 4096;
	static const uint32_t MAX_BUFFER_SIZE = 1024 * 100000;
};

}

#endif
