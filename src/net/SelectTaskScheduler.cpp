#include <cstring>
#include <forward_list>

#include "net/SelectTaskScheduler.h"
#include "net/Logger.h"
#include "net/Timer.h"

#define SELECT_CTL_ADD	0
#define SELECT_CTL_MOD  1
#define SELECT_CTL_DEL	2

using namespace xop;

SelectTaskScheduler::SelectTaskScheduler(int id) : TaskScheduler(id) {
	FD_ZERO(&fdReadBackup);
	FD_ZERO(&fdWriteBackup);
	FD_ZERO(&fdExpBackup);

	this->UpdateChannel(wakeupChannel);
}

SelectTaskScheduler::~SelectTaskScheduler() {
}

void SelectTaskScheduler::UpdateChannel(ChannelPtr channel) {
	std::lock_guard<std::mutex> lock(mutex);

	SOCKET socket = channel->GetSocket();

	if (channels.find(socket) != channels.end()) {
		if (channel->IsNoneEvent()) {
			isFdReadReset = true;
			isFdWriteReset = true;
			isFdExpReset = true;
			channels.erase(socket);
		} else {
			//isFdReadReset = true;
			isFdWriteReset = true;
		}
	} else {
		if (!channel->IsNoneEvent()) {
			channels.emplace(socket, channel);
			isFdReadReset = true;
			isFdWriteReset = true;
			isFdExpReset = true;
		}
	}
}

void SelectTaskScheduler::RemoveChannel(ChannelPtr &channel) {
	std::lock_guard<std::mutex> lock(mutex);

	SOCKET fd = channel->GetSocket();

	if(channels.find(fd) != channels.end()) {
		isFdReadReset = true;
		isFdWriteReset = true;
		isFdExpReset = true;
		channels.erase(fd);
	}
}

bool SelectTaskScheduler::HandleEvent(int timeout) {
	if (channels.empty()) {
		if (timeout <= 0)
			timeout = 10;

		Timer::Sleep(timeout);
		return true;
	}

	fd_set fdRead;
	fd_set fdWrite;
	fd_set fdExp;

	FD_ZERO(&fdRead);
	FD_ZERO(&fdWrite);
	FD_ZERO(&fdExp);

	bool fdReadReset = false;
	bool fdWriteReset = false;
	bool fdExpReset = false;

	if (isFdReadReset || isFdWriteReset || isFdExpReset) {
		if (isFdExpReset)
			maxfd = 0;

		std::lock_guard<std::mutex> lock(mutex);
		for (auto iter : channels) {
			int events = iter.second->GetEvents();
			SOCKET fd = iter.second->GetSocket();

			if (isFdReadReset && (events & EVENT_IN))
				FD_SET(fd, &fdRead);

			if (isFdWriteReset && (events & EVENT_OUT))
				FD_SET(fd, &fdWrite);

			if (isFdExpReset) {
				FD_SET(fd, &fdExp);
				if(fd > maxfd)
					maxfd = fd;
			}
		}

		fdReadReset = isFdReadReset;
		fdWriteReset = isFdWriteReset;
		fdExpReset = isFdExpReset;

		isFdReadReset = false;
		isFdWriteReset = false;
		isFdExpReset = false;
	}

	if (fdReadReset) {
		FD_ZERO(&fdReadBackup);
		memcpy(&fdReadBackup, &fdRead, sizeof(fd_set));
	} else {
		memcpy(&fdRead, &fdReadBackup, sizeof(fd_set));
	}

	if (fdWriteReset) {
		FD_ZERO(&fdWriteBackup);
		memcpy(&fdWriteBackup, &fdWrite, sizeof(fd_set));
	} else {
		memcpy(&fdWrite, &fdWriteBackup, sizeof(fd_set));
	}

	if (fdExpReset) {
		FD_ZERO(&fdExpBackup);
		memcpy(&fdExpBackup, &fdExp, sizeof(fd_set));
	} else {
		memcpy(&fdExp, &fdExpBackup, sizeof(fd_set));
	}

	if (timeout <= 0)
		timeout = 10;

	struct timeval tv = { timeout / 1000, timeout % 1000 * 1000 };
	int ret = select((int)maxfd + 1, &fdRead, &fdWrite, &fdExp, &tv);
	if (ret < 0) {
		if(errno == EINTR)
			return true;
		return false;
	}

	std::forward_list<std::pair<ChannelPtr, int>> event_list;
	if (ret > 0) {
		std::lock_guard<std::mutex> lock(mutex);
		for (auto iter : channels) {
			int events = 0;
			SOCKET socket = iter.second->GetSocket();

			if (FD_ISSET(socket, &fdRead))
				events |= EVENT_IN;

			if (FD_ISSET(socket, &fdWrite))
				events |= EVENT_OUT;

			if (FD_ISSET(socket, &fdExp))
				events |= (EVENT_HUP); // close

			if (events != 0)
				event_list.emplace_front(iter.second, events);
		}
	}

	for (auto &iter : event_list)
		iter.first->HandleEvent(iter.second);

	return true;
}
