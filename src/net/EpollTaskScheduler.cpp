#include <errno.h>
#include <sys/epoll.h>

#include "net/EpollTaskScheduler.h"

using namespace xop;

EpollTaskScheduler::EpollTaskScheduler(int id) : TaskScheduler(id) {
	epollfd = epoll_create(1024);
	this->UpdateChannel(wakeupChannel);
}

EpollTaskScheduler::~EpollTaskScheduler() {
	if (epollfd >= 0) {
		close(epollfd);
		epollfd = -1;
	}
}

void EpollTaskScheduler::UpdateChannel(ChannelPtr channel) {
	std::lock_guard<std::mutex> lock(mutex);
	int fd = channel->GetSocket();

	if (channels.find(fd) != channels.end()) {
		if (channel->IsNoneEvent()) {
			Update(EPOLL_CTL_DEL, channel);
			channels.erase(fd);
		} else {
			Update(EPOLL_CTL_MOD, channel);
		}
	} else {
		if (!channel->IsNoneEvent()) {
			channels.emplace(fd, channel);
			Update(EPOLL_CTL_ADD, channel);
		}	
	}	
}

void EpollTaskScheduler::Update(int operation, ChannelPtr &channel) {
	struct epoll_event event = { 0 };

	if (operation != EPOLL_CTL_DEL) {
		event.data.ptr = channel.get();
		event.events = channel->GetEvents();
	}

	if (::epoll_ctl(epollfd, operation, channel->GetSocket(), &event) < 0) {
	}
}

void EpollTaskScheduler::RemoveChannel(ChannelPtr &channel) {
	std::lock_guard<std::mutex> lock(mutex);
	int fd = channel->GetSocket();

	if (channels.find(fd) != channels.end()) {
		Update(EPOLL_CTL_DEL, channel);
		channels.erase(fd);
	}
}

bool EpollTaskScheduler::HandleEvent(int timeout) {
	struct epoll_event events[512] = { 0 };
	int num_events = -1;

	num_events = epoll_wait(epollfd, events, 512, timeout);
	if (num_events < 0) {
		if (errno != EINTR)
			return false;
	}

	for (int n = 0; n < num_events; n++) {
		if (events[n].data.ptr) {
			((Channel *)events[n].data.ptr)->HandleEvent(events[n].events);
		}
	}

	return true;
}
