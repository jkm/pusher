#include <cstdio>

#include "net/Acceptor.h"
#include "net/EventLoop.h"
#include "net/Logger.h"
#include "net/TcpServer.h"

using namespace xop;

TcpServer::TcpServer(EventLoop *loop) : loop(loop), port(0), acceptor(new Acceptor(loop)), isStarted(false) {
	acceptor->SetNewConnectionCallback([this](SOCKET sockFd) {
		TcpConnection::Ptr conn = this->OnConnect(sockFd);
		if (conn) {
			this->AddConnection(sockFd, conn);
			conn->SetDisconnectCallback([this](TcpConnection::Ptr conn) {
				auto scheduler = conn->GetTaskScheduler();
				SOCKET sockFd = conn->GetSocket();
				if (!scheduler->AddTriggerEvent([this, sockFd] {this->RemoveConnection(sockFd); })) {
					scheduler->AddTimer([this, sockFd]() {
						this->RemoveConnection(sockFd);
						return false;
					}, 100);
				}
			});
		}
	});
}

TcpServer::~TcpServer() {
	Stop();
}

bool TcpServer::Start(std::string ip, uint16_t port) {
	Stop();

	if (!isStarted) {
		if (acceptor->Listen(ip, port) < 0)
			return false;

		this->port = port;
		this->ip = ip;
		this->isStarted = true;

		return true;
	}

	return false;
}

void TcpServer::Stop() {
	if (isStarted) {
		mutex.lock();
		for (auto iter : connections)
			iter.second->Disconnect();
		mutex.unlock();

		acceptor->Close();
		isStarted = false;

		while (1) {
			Timer::Sleep(10);
			if (connections.empty())
				break;
		}
	}
}

TcpConnection::Ptr TcpServer::OnConnect(SOCKET sockFd) {
	return std::make_shared<TcpConnection>(loop->GetTaskScheduler().get(), sockFd);
}

void TcpServer::AddConnection(SOCKET sockFd, TcpConnection::Ptr tcpConn) {
	std::lock_guard<std::mutex> locker(mutex);
	connections.emplace(sockFd, tcpConn);
}

void TcpServer::RemoveConnection(SOCKET sockFd) {
	std::lock_guard<std::mutex> locker(mutex);
	connections.erase(sockFd);
}
