#include "net/Logger.h"
#include "net/TcpSocket.h"
#include "net/Timestamp.h"
#include "xop/RtspConnection.h"
#include "xop/RtspPusher.h"

using namespace xop;

RtspPusher::RtspPusher(xop::EventLoop *loop) : loop(loop) {
}

RtspPusher::~RtspPusher() {
	this->Close();
}

std::shared_ptr<RtspPusher> RtspPusher::Create(xop::EventLoop *loop) {
	std::shared_ptr<RtspPusher> pusher(new RtspPusher(loop));
	return pusher;
}

void RtspPusher::AddSession(MediaSession *session) {
	std::lock_guard<std::mutex> locker(mutex);
	this->session.reset(session);
}

void RtspPusher::RemoveSession(MediaSessionId sessionId) {
	std::lock_guard<std::mutex> locker(mutex);
	this->session = nullptr;
}

MediaSession::Ptr RtspPusher::LookMediaSession(MediaSessionId sessionId) {
	return this->session;
}

int RtspPusher::OpenUrl(std::string url, int msec) {
	std::lock_guard<std::mutex> lock(mutex);

	static xop::Timestamp timestamp;
	int timeout = msec;
	if (timeout <= 0)
		timeout = 10000;

	timestamp.Reset();

	if (!this->ParseRtspUrl(url)) {
		LOG_ERROR("rtsp url(%s) was illegal.\n", url.c_str());
		return -1;
	}

	if (conn != nullptr) {
		std::shared_ptr<RtspConnection> rtspConn = conn;
		SOCKET sockFd = rtspConn->GetSocket();
		scheduler->AddTriggerEvent([sockFd, rtspConn]() {
			rtspConn->Disconnect();
		});
		conn = nullptr;
	}

	TcpSocket tcpSocket;
	tcpSocket.Create();
	if (!tcpSocket.Connect(rtspUrl.ip, rtspUrl.port, timeout)) {
		tcpSocket.Close();
		return -1;
	}

	scheduler = loop->GetTaskScheduler().get();
	conn.reset(new RtspConnection(shared_from_this(), scheduler, tcpSocket.GetSocket()));

	loop->AddTriggerEvent([this]() {
		conn->SendOptions(RtspConnection::RTSP_PUSHER);
	});

	timeout -= (int)timestamp.Elapsed();
	if (timeout < 0)
		timeout = 1000;

	do {
		xop::Timer::Sleep(100);
		timeout -= 100;
	} while (!conn->IsRecord() && timeout > 0);

	if (!conn->IsRecord()) {
		std::shared_ptr<RtspConnection> rtspConn = conn;
		SOCKET sockFd = rtspConn->GetSocket();
		scheduler->AddTriggerEvent([sockFd, rtspConn]() {
			rtspConn->Disconnect();
		});
		conn = nullptr;
		return -1;
	}

	return 0;
}

void RtspPusher::Close() {
	std::lock_guard<std::mutex> lock(mutex);

	if (conn != nullptr) {
		std::shared_ptr<RtspConnection> rtspConn = conn;
		SOCKET sockFd = rtspConn->GetSocket();
		scheduler->AddTriggerEvent([sockFd, rtspConn]() {
			rtspConn->Disconnect();
		});
		conn = nullptr;
	}
}

bool RtspPusher::IsConnected() {
	std::lock_guard<std::mutex> lock(mutex);

	if (conn != nullptr)
		return !conn->IsClosed();

	return false;
}

bool RtspPusher::PushFrame(MediaChannelId channelId, AVFrame frame) {
	std::lock_guard<std::mutex> locker(mutex);

	if (!session || !conn)
		return false;

	return session->HandleFrame(channelId, frame);
}
