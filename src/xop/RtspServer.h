#ifndef XOP_RTSP_SERVER_H
#define XOP_RTSP_SERVER_H

#include <memory>
#include <string>
#include <mutex>
#include <unordered_map>

#include "net/TcpServer.h"
#include "xop/rtsp.h"


namespace xop {

class RtspConnection;

class RtspServer : public Rtsp, public TcpServer {
public:    
	static std::shared_ptr<RtspServer> Create(xop::EventLoop *loop);
	~RtspServer();

	MediaSessionId AddSession(MediaSession *session);
	void RemoveSession(MediaSessionId sessionId);
	bool PushFrame(MediaSessionId sessionId, MediaChannelId channelId, AVFrame frame);

private:
	friend class RtspConnection;

	RtspServer(xop::EventLoop *loop);
	MediaSession::Ptr LookMediaSession(const std::string &suffix);
	MediaSession::Ptr LookMediaSession(MediaSessionId sessionId);
	virtual TcpConnection::Ptr OnConnect(SOCKET sockFd);

	std::mutex mutex;
	std::unordered_map<MediaSessionId, std::shared_ptr<MediaSession>> media_sessions;
	std::unordered_map<std::string, MediaSessionId> rtsp_suffix_map;
};

}

#endif
