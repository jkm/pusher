#include <chrono>
#include <cstdio>
#include <sys/time.h>

#include "xop/H264Source.h"

using namespace xop;

H264Source::H264Source(uint32_t frameRate) : frameRate(frameRate) {
	payload		= 96;
	mediaType	= H264;
	clockRate	= 90000;
}

H264Source *H264Source::CreateNew(uint32_t frameRate) {
	return new H264Source(frameRate);
}

H264Source::~H264Source() {
}

std::string H264Source::GetMediaDescription(uint16_t port) {
	char buf[100] = { 0 };
	sprintf(buf, "m=video %hu RTP/AVP 96", port);

	return std::string(buf);
}

std::string H264Source::GetAttribute() {
	return std::string("a=rtpmap:96 H264/90000");
}

bool H264Source::HandleFrame(MediaChannelId channelId, AVFrame frame) {
	uint8_t *frameBuf = frame.buffer.get();
	uint32_t frameSize = frame.size;

	if (frame.timestamp == 0)
		frame.timestamp = GetTimestamp();

	if (frameSize <= MAX_RTP_PAYLOAD_SIZE) {
		RtpPacket pkt;
		pkt.type = frame.type;
		pkt.timestamp = frame.timestamp;
		pkt.size = frameSize + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE;
		pkt.last = 1;

		memcpy(pkt.data.get() + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE, frameBuf, frameSize);

		if (cb) {
			if (!cb(channelId, pkt))
				return false;
		}
	} else {
		char FU_A[2] = { 0 };

		FU_A[0] = (frameBuf[0] & 0xE0) | 28;
		FU_A[1] = 0x80 | (frameBuf[0] & 0x1f);

		frameBuf  += 1;
		frameSize -= 1;

		while (frameSize + 2 > MAX_RTP_PAYLOAD_SIZE) {
			RtpPacket pkt;
			pkt.type = frame.type;
			pkt.timestamp = frame.timestamp;
			pkt.size = RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + MAX_RTP_PAYLOAD_SIZE;
			pkt.last = 0;

			pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 0] = FU_A[0];
			pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 1] = FU_A[1];
			memcpy(pkt.data.get() + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 2, frameBuf, MAX_RTP_PAYLOAD_SIZE - 2);

			if (cb) {
				if (!cb(channelId, pkt))
					return false;
			}

			frameBuf  += MAX_RTP_PAYLOAD_SIZE - 2;
			frameSize -= MAX_RTP_PAYLOAD_SIZE - 2;

			FU_A[1] &= ~0x80;
		}

		{
			RtpPacket pkt;
			pkt.type = frame.type;
			pkt.timestamp = frame.timestamp;
			pkt.size = 4 + RTP_HEADER_SIZE + 2 + frameSize;
			pkt.last = 1;

			FU_A[1] |= 0x40;
			pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 0] = FU_A[0];
			pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 1] = FU_A[1];
			memcpy(pkt.data.get() + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 2, frameBuf, frameSize);

			if (cb) {
				if (!cb(channelId, pkt))
					return false;
			}
		}
	}

	return true;
}

uint32_t H264Source::GetTimestamp() {
	struct timeval tv = { 0 };
	gettimeofday(&tv, NULL);
	uint32_t ts = ((tv.tv_sec * 1000) + ((tv.tv_usec + 500) / 1000)) * 90; // 90: clockRate / 1000;

	return ts;
}
