#include <chrono>
#include <cstdio>
#include <sys/time.h>

#include "xop/G711ASource.h"

using namespace xop;

G711ASource::G711ASource() {
	payload		= 8;
	mediaType	= PCMA;
	clockRate	= 8000;
}

G711ASource *G711ASource::CreateNew() {
	return new G711ASource();
}

G711ASource::~G711ASource() {
}

std::string G711ASource::GetMediaDescription(uint16_t port) {
	char buf[100] = { 0 };
	sprintf(buf, "m=audio %hu RTP/AVP 8", port);

	return std::string(buf);
}

std::string G711ASource::GetAttribute() {
	return std::string("a=rtpmap:8 PCMA/8000/1");
}

bool G711ASource::HandleFrame(MediaChannelId channelId, AVFrame frame) {
	if (frame.size > MAX_RTP_PAYLOAD_SIZE)
		return false;

	uint8_t *frameBuf  = frame.buffer.get();
	uint32_t frameSize = frame.size;

	RtpPacket pkt;
	pkt.type = frame.type;
	pkt.timestamp = frame.timestamp;
	pkt.size = frameSize + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE;
	pkt.last = 1;

	memcpy(pkt.data.get() + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE, frameBuf, frameSize);

	if (cb)
		cb(channelId, pkt);

	return true;
}

uint32_t G711ASource::GetTimestamp() {
	auto timePoint = std::chrono::time_point_cast<std::chrono::microseconds>(std::chrono::steady_clock::now());
	return (uint32_t)((timePoint.time_since_epoch().count() + 500)/ 1000 * 8);
}
