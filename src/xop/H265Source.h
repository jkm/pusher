#ifndef XOP_H265_SOURCE_H
#define XOP_H265_SOURCE_H

#include "xop/MediaSource.h"
#include "xop/rtp.h"

namespace xop {

class H265Source : public MediaSource {
public:
	static H265Source *CreateNew(uint32_t frameRate = 25);
	~H265Source();

	void SetFrameRate(uint32_t frameRate) {
		this->frameRate = frameRate;
	}

	uint32_t GetFrameRate() const {
		return frameRate;
	}

	virtual std::string GetMediaDescription(uint16_t port);
	virtual std::string GetAttribute();

	virtual bool HandleFrame(MediaChannelId channelId, AVFrame frame);

	static uint32_t GetTimestamp();

private:
	H265Source(uint32_t frameRate);

	uint32_t frameRate = 25;
};

}

#endif
