#ifndef _RTSP_CONNECTION_H
#define _RTSP_CONNECTION_H

#include <cstdint>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

#include "net/EventLoop.h"
#include "net/TcpConnection.h"
#include "xop/RtpConnection.h"
#include "xop/RtspMessage.h"
#include "xop/rtsp.h"

namespace xop {

class RtspServer;
class MediaSession;

class RtspConnection : public TcpConnection {
public:
	using CloseCallback = std::function<void (SOCKET sockFd)>;

	enum ConnectionMode {
		RTSP_SERVER,
		RTSP_PUSHER,
		RTSP_CLIENT
	};

	enum ConnectionState {
		START_CONNECT,
		START_PLAY,
		START_PUSH
	};

	RtspConnection() = delete;
	RtspConnection(std::shared_ptr<Rtsp> server, TaskScheduler *scheduler, SOCKET sockFd);
	virtual ~RtspConnection();

	MediaSessionId GetMediaSessionId() {
		return sessionId;
	}

	TaskScheduler *GetTaskScheduler() const {
		return scheduler;
	}

	void KeepAlive() {
		aliveCount++;
	}

	bool IsAlive() const {
		if (IsClosed())
			return false;

		if (conn != nullptr) {
			if (conn->IsMulticast())
				return true;
		}

		return aliveCount > 0;
	}

	void ResetAliveCount() {
		aliveCount = 0;
	}

	int GetId() const {
		return scheduler->GetId();
	}

	bool IsPlay() const {
		return state == START_PLAY;
	}

	bool IsRecord() const {
		return state == START_PUSH;
	}

private:
	friend class RtpConnection;
	friend class MediaSession;
	friend class RtspServer;
	friend class RtspPusher;

	bool OnRead(BufferReader &buffer);
	void OnClose();
	void HandleRtcp(SOCKET sockFd);
	void HandleRtcp(BufferReader &buffer);
	bool HandleRtspRequest(BufferReader &buffer);
	bool HandleRtspResponse(BufferReader &buffer);

	void SendRtspMessage(std::shared_ptr<char> buf, uint32_t size);

	void HandleCmdOption();
	void HandleCmdDescribe();
	void HandleCmdSetup();
	void HandleCmdPlay();
	void HandleCmdTeardown();
	void HandleCmdGetParamter();
	bool HandleAuthentication();

	void SendOptions(ConnectionMode mode = RTSP_SERVER);
	void SendDescribe();
	void SendAnnounce();
	void SendSetup();
	void HandleRecord();

	std::atomic_int aliveCount;
	std::weak_ptr<Rtsp> rtspPtr;
	xop::TaskScheduler *scheduler = nullptr;

	ConnectionMode mode = RTSP_SERVER;
	ConnectionState state = START_CONNECT;
	MediaSessionId sessionId = 0;

	std::string nonce;

	std::shared_ptr<Channel> rtpChannel;
	std::shared_ptr<Channel> rtcpChannels[MAX_MEDIA_CHANNEL];
	std::unique_ptr<RtspRequest> request;
	std::unique_ptr<RtspResponse> response;
	std::shared_ptr<RtpConnection> conn;
};

}

#endif
