#ifndef XOP_RTSP_H
#define XOP_RTSP_H

#include <cstdio>
#include <string>

#include "net/Acceptor.h"
#include "net/EventLoop.h"
#include "net/Socket.h"
#include "net/Timer.h"
#include "xop/MediaSession.h"

namespace xop {

struct RtspUrl {
	std::string url;
	std::string ip;
	uint16_t port;
	std::string suffix;
};

class Rtsp : public std::enable_shared_from_this<Rtsp> {
public:
	Rtsp() {}
	virtual ~Rtsp() {}

	virtual void SetAuthConfig(std::string realm, std::string username, std::string password) {
		this->realm = realm;
		this->username = username;
		this->password = password;
	}

	virtual void SetVersion(std::string version) {
		this->version = std::move(version);
	}

	virtual std::string GetVersion() {
		return version;
	}

	virtual std::string GetRtspUrl() {
		return rtspUrl.url;
	}

	bool ParseRtspUrl(std::string url) {
		char ip[100] = { 0 };
		char suffix[100] = { 0 };
		uint16_t port = 0;

		if (sscanf(url.c_str() + 7, "%[^:]:%hu/%s", ip, &port, suffix) == 3)
			rtspUrl.port = port;
		else if (sscanf(url.c_str() + 7, "%[^/]/%s", ip, suffix) == 2)
			rtspUrl.port = 554;
		else
			return false;

		rtspUrl.ip = ip;
		rtspUrl.suffix = suffix;
		rtspUrl.url = url;

		return true;
	}

protected:
	friend class RtspConnection;

	virtual MediaSession::Ptr LookMediaSession(const std::string &suffix) {
		return nullptr;
	}

	virtual MediaSession::Ptr LookMediaSession(MediaSessionId sessionId) {
		return nullptr;
	}

	std::string realm;
	std::string username;
	std::string password;
	std::string version;
	struct RtspUrl rtspUrl;
};

}

#endif
