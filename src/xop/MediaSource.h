#ifndef XOP_MEDIA_SOURCE_H
#define XOP_MEDIA_SOURCE_H

#include <cstdint>
#include <functional>
#include <map>
#include <memory>
#include <string>

#include "xop/media.h"
#include "xop/rtp.h"
#include "net/Socket.h"

namespace xop {

class MediaSource {
public:
	using SendFrameCallback = std::function<bool (MediaChannelId channelId, RtpPacket pkt)>;

	MediaSource() {}
	virtual ~MediaSource() {}

	virtual MediaType GetMediaType() const {
		return mediaType;
	}

	virtual std::string GetMediaDescription(uint16_t port = 0) = 0;

	virtual std::string GetAttribute() = 0;

	virtual bool HandleFrame(MediaChannelId channelId, AVFrame frame) = 0;

	virtual void SetSendFrameCallback(const SendFrameCallback callback) {
		cb = callback;
	}

	virtual uint32_t GetPayload() const {
		return payload;
	}

	virtual uint32_t GetClockRate() const {
		return clockRate;
	}

protected:
	MediaType mediaType = NONE;

	uint32_t payload = 0;
	uint32_t clockRate = 0;

	SendFrameCallback cb;
};

}

#endif
