#ifndef XOP_RTSP_PUSHER_H
#define XOP_RTSP_PUSHER_H

#include <mutex>

#include "rtsp.h"

namespace xop {

class RtspConnection;

class RtspPusher : public Rtsp {
public:
	static std::shared_ptr<RtspPusher> Create(xop::EventLoop *loop);
	~RtspPusher();

	void AddSession(MediaSession *session);
	void RemoveSession(MediaSessionId sessionId);

	int  OpenUrl(std::string url, int msec);
	void Close();
	bool IsConnected();

	bool PushFrame(MediaChannelId channelId, AVFrame frame);

private:
	friend class RtspConnection;

	RtspPusher(xop::EventLoop *loop);
	MediaSession::Ptr LookMediaSession(MediaSessionId sessionId);

	xop::EventLoop *loop = nullptr;
	xop::TaskScheduler *scheduler = nullptr;
	std::mutex mutex;
	std::shared_ptr<RtspConnection> conn;
	std::shared_ptr<MediaSession> session;
};

}

#endif
