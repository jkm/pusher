#include "net/SocketUtil.h"
#include "xop/RtpConnection.h"
#include "xop/RtspConnection.h"

using namespace xop;

RtpConnection::RtpConnection(std::weak_ptr<TcpConnection> rtspConn) : rtspConn(rtspConn) {
	std::random_device rd;

	for (int ch = 0; ch < MAX_MEDIA_CHANNEL; ch++) {
		rtpFds[ch] = 0;
		rtcpFds[ch] = 0;

		memset(&info[ch], 0, sizeof(info[ch]));
		info[ch].rtp_header.version = RTP_VERSION;
		info[ch].packet_seq = rd() & 0xffff;
		info[ch].rtp_header.seq = 0; // htons(1);
		info[ch].rtp_header.ts = htonl(rd());
		info[ch].rtp_header.ssrc = htonl(rd());
	}

	auto conn = rtspConn.lock();
	ip = conn->GetIp();
	port = conn->GetPort();
}

RtpConnection::~RtpConnection() {
	for (int ch = 0; ch < MAX_MEDIA_CHANNEL; ch++) {
		if (rtpFds[ch] > 0)
			SocketUtil::Close(rtpFds[ch]);

		if (rtcpFds[ch] > 0)
			SocketUtil::Close(rtcpFds[ch]);
	}
}

int RtpConnection::GetId() const {
	auto conn = rtspConn.lock();
	if (!conn)
		return -1;

	RtspConnection *rtspConn = (RtspConnection *)conn.get();

	return rtspConn->GetId();
}

bool RtpConnection::SetupRtpOverTcp(MediaChannelId channelId, uint16_t rtpChannel, uint16_t rtcpChannel) {
	auto conn = rtspConn.lock();
	if (!conn)
		return false;

	info[channelId].rtp_channel = rtpChannel;
	info[channelId].rtcp_channel = rtcpChannel;
	rtpFds[channelId] = conn->GetSocket();
	rtcpFds[channelId] = conn->GetSocket();
	info[channelId].isSetup = true;

	transportMode = RTP_OVER_TCP;

	return true;
}

bool RtpConnection::SetupRtpOverUdp(MediaChannelId channelId, uint16_t rtpPort, uint16_t rtcpPort) {
	auto conn = rtspConn.lock();
	if (!conn)
		return false;

	if (SocketUtil::GetPeerAddr(conn->GetSocket(), &addr) < 0)
		return false;

	info[channelId].rtp_port = rtpPort;
	info[channelId].rtcp_port = rtcpPort;

	std::random_device rd;
	for (int n = 0; n <= 10; n++) {
		if (n == 10)
			return false;

		rtpPorts[channelId] = rd() & 0xfffe;
		rtcpPorts[channelId] = rtpPorts[channelId] + 1;

		rtpFds[channelId] = ::socket(AF_INET, SOCK_DGRAM, 0);
		if (!SocketUtil::Bind(rtpFds[channelId], "0.0.0.0",  rtpPorts[channelId])) {
			SocketUtil::Close(rtpFds[channelId]);
			continue;
		}

		rtcpFds[channelId] = ::socket(AF_INET, SOCK_DGRAM, 0);
		if (!SocketUtil::Bind(rtcpFds[channelId], "0.0.0.0", rtcpPorts[channelId])) {
			SocketUtil::Close(rtpFds[channelId]);
			SocketUtil::Close(rtcpFds[channelId]);
			continue;
		}

		break;
	}

	SocketUtil::SetSendBufSize(rtpFds[channelId], 50 * 1024);

	rtpAddrs[channelId].sin_family = AF_INET;
	rtpAddrs[channelId].sin_addr.s_addr = addr.sin_addr.s_addr;
	rtpAddrs[channelId].sin_port = htons(info[channelId].rtp_port);

	rtcpAddrs[channelId].sin_family = AF_INET;
	rtcpAddrs[channelId].sin_addr.s_addr = addr.sin_addr.s_addr;
	rtcpAddrs[channelId].sin_port = htons(info[channelId].rtcp_port);

	info[channelId].isSetup = true;

	transportMode = RTP_OVER_UDP;

	return true;
}

bool RtpConnection::SetupRtpOverMulticast(MediaChannelId channelId, std::string ip, uint16_t port) {
	std::random_device rd;
	for (int n = 0; n <= 10; n++) {
		if (n == 10)
			return false;

		rtpPorts[channelId] = rd() & 0xfffe;
		rtpFds[channelId] = ::socket(AF_INET, SOCK_DGRAM, 0);

		if (!SocketUtil::Bind(rtpFds[channelId], "0.0.0.0", rtpPorts[channelId])) {
			SocketUtil::Close(rtpFds[channelId]);
			continue;
		}

		break;
	}

	info[channelId].rtp_port = port;

	rtpAddrs[channelId].sin_family = AF_INET;
	rtpAddrs[channelId].sin_addr.s_addr = inet_addr(ip.c_str());
	rtpAddrs[channelId].sin_port = htons(port);

	info[channelId].isSetup = true;

	transportMode = RTP_OVER_MULTICAST;

	isMulticast = true;

	return true;
}

void RtpConnection::Play() {
	for (int ch = 0; ch < MAX_MEDIA_CHANNEL; ch++) {
		if (info[ch].isSetup)
			info[ch].isPlay = true;
	}
}

void RtpConnection::Record() {
	for (int ch = 0; ch < MAX_MEDIA_CHANNEL; ch++) {
		if (info[ch].isSetup) {
			info[ch].isRecord = true;
			info[ch].isPlay = true;
		}
	}
}

void RtpConnection::Teardown() {
	if (!isClosed) {
		isClosed = true;
		for (int ch = 0; ch < MAX_MEDIA_CHANNEL; ch++) {
			info[ch].isPlay = false;
			info[ch].isRecord = false;
		}
	}
}

std::string RtpConnection::GetMulticastIp(MediaChannelId channelId) const {
	return std::string(inet_ntoa(rtpAddrs[channelId].sin_addr));
}

std::string RtpConnection::GetRtpInfo(const std::string &url) {
	char buf[2048] = { 0 };
	snprintf(buf, 1024, "RTP-Info: ");

	int num_channel = 0;

	auto timePoint = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now());
	auto ts = timePoint.time_since_epoch().count();
	for (int ch = 0; ch < MAX_MEDIA_CHANNEL; ch++) {
		uint32_t rtpTime = (uint32_t)(ts * info[ch].clock_rate / 1000);
		if (info[ch].isSetup) {
			if (num_channel != 0)
				snprintf(buf + strlen(buf), sizeof(buf) - strlen(buf), ",");

			snprintf(buf + strlen(buf),
				 sizeof(buf) - strlen(buf),
				 "url=%s/track%d;seq=0;rtptime=%u",
				 url.c_str(),
				 ch,
				 rtpTime);

			num_channel++;
		}
	}

	return std::string(buf);
}

void RtpConnection::SetFrameType(uint8_t frameType) {
	if (!hasKeyFrame && (frameType == 0 || frameType == VIDEO_FRAME_I))
		hasKeyFrame = true;
}

void RtpConnection::SetRtpHeader(MediaChannelId channelId, RtpPacket pkt) {
	if ((info[channelId].isPlay || info[channelId].isRecord) && hasKeyFrame) {
		info[channelId].rtp_header.marker = pkt.last;
		info[channelId].rtp_header.ts = htonl(pkt.timestamp);
		info[channelId].rtp_header.seq = htons(info[channelId].packet_seq++);
		memcpy(pkt.data.get() + 4, &info[channelId].rtp_header, RTP_HEADER_SIZE);
	}
}

int RtpConnection::SendRtpPacket(MediaChannelId channelId, RtpPacket pkt) {
	if (isClosed)
		return -1;

	auto conn = rtspConn.lock();
	if (!conn)
		return -1;

	RtspConnection *rtsp_conn = (RtspConnection *)conn.get();

	bool ret = rtsp_conn->scheduler->AddTriggerEvent([this, channelId, pkt] {
		this->SetFrameType(pkt.type);
		this->SetRtpHeader(channelId, pkt);
		if ((info[channelId].isPlay || info[channelId].isRecord) && hasKeyFrame) {
			if (transportMode == RTP_OVER_TCP)
				SendRtpOverTcp(channelId, pkt);
			else
				SendRtpOverUdp(channelId, pkt);

			// info[channelId].octetCount  += pkt.size;
			// info[channelId].packetCount += 1;
		}
	});

	return ret ? 0 : -1;
}

int RtpConnection::SendRtpOverTcp(MediaChannelId channelId, RtpPacket pkt) {
	auto conn = rtspConn.lock();
	if (!conn)
		return -1;

	uint8_t *rtpPktPtr = pkt.data.get();
	rtpPktPtr[0] = '$';
	rtpPktPtr[1] = (char)info[channelId].rtp_channel;
	rtpPktPtr[2] = (char)(((pkt.size - 4) & 0xff00) >> 8);
	rtpPktPtr[3] = (char)((pkt.size - 4) & 0xff);

	conn->Send((char *)rtpPktPtr, pkt.size);

	return pkt.size;
}

int RtpConnection::SendRtpOverUdp(MediaChannelId channelId, RtpPacket pkt) {
	int ret = sendto(rtpFds[channelId],
			 (const char *)pkt.data.get() + 4,
			 pkt.size - 4,
			 0,
			 (struct sockaddr *)&(rtpAddrs[channelId]),
			 sizeof(struct sockaddr_in));

	if (ret < 0) {
		Teardown();
		return -1;
	}

	return ret;
}
