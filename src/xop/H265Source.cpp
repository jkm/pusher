#include <chrono>
#include <cstdio>
#include <sys/time.h>

#include "xop/H265Source.h"

using namespace xop;

H265Source::H265Source(uint32_t frameRate) : frameRate(frameRate) {
	payload		= 96;
	mediaType	= H265;
	clockRate	= 90000;
}

H265Source *H265Source::CreateNew(uint32_t frameRate) {
	return new H265Source(frameRate);
}

H265Source::~H265Source() {
}

std::string H265Source::GetMediaDescription(uint16_t port) {
	char buf[100] = { 0 };
	sprintf(buf, "m=video %hu RTP/AVP 96", port);

	return std::string(buf);
}

std::string H265Source::GetAttribute() {
	return std::string("a=rtpmap:96 H265/90000");
}

bool H265Source::HandleFrame(MediaChannelId channelId, AVFrame frame) {
	uint8_t *frameBuf = frame.buffer.get();
	uint32_t frameSize = frame.size;

	if (frame.timestamp == 0)
		frame.timestamp = GetTimestamp();

	if (frameSize <= MAX_RTP_PAYLOAD_SIZE) {
		RtpPacket pkt;
		pkt.type = frame.type;
		pkt.timestamp = frame.timestamp;
		pkt.size = frameSize + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE;
		pkt.last = 1;

		memcpy(pkt.data.get() + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE, frameBuf, frameSize);

		if (cb) {
			if (!cb(channelId, pkt))
				return false;
		}
	} else {
		char FU[3] = { 0 };
		char nalUnitType = (frameBuf[0] & 0x7e) >> 1;

		FU[0] = (frameBuf[0] & 0x81) | (49 << 1);
		FU[1] = frameBuf[1];
		FU[2] = (0x80 | nalUnitType);
        
		frameBuf  += 2;
		frameSize -= 2;
        
		while (frameSize + 3 > MAX_RTP_PAYLOAD_SIZE) {
			RtpPacket pkt;
			pkt.type = frame.type;
			pkt.timestamp = frame.timestamp;
			pkt.size = RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + MAX_RTP_PAYLOAD_SIZE;
			pkt.last = 0;

			pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 0] = FU[0];
			pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 1] = FU[1];
			pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 2] = FU[2];
			memcpy(pkt.data.get() + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 3, frameBuf, MAX_RTP_PAYLOAD_SIZE - 3);
            
			if (cb) {
				if (!cb(channelId, pkt))
					return false;
			}

			frameBuf  += (MAX_RTP_PAYLOAD_SIZE - 3);
			frameSize -= (MAX_RTP_PAYLOAD_SIZE - 3);

			FU[2] &= ~0x80;
		}

		{
			RtpPacket pkt;
			pkt.type = frame.type;
			pkt.timestamp = frame.timestamp;
			pkt.size = RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 3 + frameSize;
			pkt.last = 1;

			FU[2] |= 0x40;

			pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 0] = FU[0];
			pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 1] = FU[1];
			pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 2] = FU[2];
			memcpy(pkt.data.get() + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 3, frameBuf, frameSize);

			if (cb) {
				if (!cb(channelId, pkt))
					return false;
			}
		}
	}

	return true;
}

uint32_t H265Source::GetTimestamp() {
	struct timeval tv = { 0 };
	gettimeofday(&tv, NULL);
	uint32_t ts = ((tv.tv_sec * 1000) + ((tv.tv_usec + 500) / 1000)) * 90; // 90: clockRate / 1000;

	return ts;
}
