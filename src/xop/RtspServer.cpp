#include "net/SocketUtil.h"
#include "net/Logger.h"
#include "xop/RtspConnection.h"
#include "xop/RtspServer.h"

using namespace xop;

RtspServer::RtspServer(EventLoop *loop) : TcpServer(loop) {
}

RtspServer::~RtspServer() {
}

std::shared_ptr<RtspServer> RtspServer::Create(xop::EventLoop *loop) {
	std::shared_ptr<RtspServer> server(new RtspServer(loop));
	return server;
}

MediaSessionId RtspServer::AddSession(MediaSession* session) {
	std::lock_guard<std::mutex> locker(mutex);

	if (rtsp_suffix_map.find(session->GetRtspUrlSuffix()) != rtsp_suffix_map.end())
		return 0;

	std::shared_ptr<MediaSession> media_session(session);
	MediaSessionId sessionId = media_session->GetMediaSessionId();
	rtsp_suffix_map.emplace(std::move(media_session->GetRtspUrlSuffix()), sessionId);
	media_sessions.emplace(sessionId, std::move(media_session));

	return sessionId;
}

void RtspServer::RemoveSession(MediaSessionId sessionId) {
	std::lock_guard<std::mutex> locker(mutex);

	auto iter = media_sessions.find(sessionId);
	if (iter != media_sessions.end()) {
		rtsp_suffix_map.erase(iter->second->GetRtspUrlSuffix());
		media_sessions.erase(sessionId);
	}
}

MediaSession::Ptr RtspServer::LookMediaSession(const std::string &suffix) {
	std::lock_guard<std::mutex> locker(mutex);

	auto iter = rtsp_suffix_map.find(suffix);
	if (iter != rtsp_suffix_map.end()) {
		MediaSessionId id = iter->second;
		return media_sessions[id];
	}

	return nullptr;
}

MediaSession::Ptr RtspServer::LookMediaSession(MediaSessionId sessionId) {
	std::lock_guard<std::mutex> locker(mutex);

	auto iter = media_sessions.find(sessionId);
	if (iter != media_sessions.end())
		return iter->second;

	return nullptr;
}

bool RtspServer::PushFrame(MediaSessionId sessionId, MediaChannelId channelId, AVFrame frame) {
	std::shared_ptr<MediaSession> sessionPtr = nullptr;

	{
		std::lock_guard<std::mutex> locker(mutex);
		auto iter = media_sessions.find(sessionId);
		if (iter != media_sessions.end())
			sessionPtr = iter->second;
		else
			return false;
	}

	if (sessionPtr != nullptr && sessionPtr->GetNumClient() != 0)
		return sessionPtr->HandleFrame(channelId, frame);

	return false;
}

TcpConnection::Ptr RtspServer::OnConnect(SOCKET sockFd) {
	return std::make_shared<RtspConnection>(shared_from_this(), loop->GetTaskScheduler().get(), sockFd);
}
