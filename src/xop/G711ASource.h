#ifndef XOP_G711A_SOURCE_H
#define XOP_G711A_SOURCE_H

#include "xop/MediaSource.h"
#include "xop/rtp.h"

namespace xop {

class G711ASource : public MediaSource {
public:
	static G711ASource *CreateNew();
	virtual ~G711ASource();

	uint32_t GetSampleRate() const {
		return sampleRate;
	}

	uint32_t GetChannels() const {
		return channels;
	}

	virtual std::string GetMediaDescription(uint16_t port = 0);

	virtual std::string GetAttribute();

	virtual bool HandleFrame(MediaChannelId channelId, AVFrame frame);

	static uint32_t GetTimestamp();

private:
	G711ASource();

	uint32_t sampleRate = 8000;
	uint32_t channels = 1;
};

}

#endif
