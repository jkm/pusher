#ifndef XOP_MEDIA_SESSION_H
#define XOP_MEDIA_SESSION_H

#include <atomic>
#include <cstdint>
#include <memory>
#include <mutex>
#include <random>
#include <string>
#include <unordered_set>
#include <vector>

#include "xop/media.h"
#include "xop/H264Source.h"
#include "xop/H265Source.h"
#include "xop/VP8Source.h"
#include "xop/G711ASource.h"
#include "xop/AACSource.h"
#include "xop/MediaSource.h"
#include "net/Socket.h"
#include "net/RingBuffer.h"

namespace xop {

class RtpConnection;

class MediaSession {
public:
	using Ptr = std::shared_ptr<MediaSession>;
	using NotifyConnectedCallback = std::function<void (MediaSessionId sessionId, std::string peerIp, uint16_t peerPort)>;
	using NotifyDisconnectedCallback = std::function<void (MediaSessionId sessionId, std::string peerIp, uint16_t peerPort)>;

	static MediaSession *CreateNew(std::string suffix = "live");
	virtual ~MediaSession();

	bool AddSource(MediaChannelId channelId, MediaSource *source);
	bool RemoveSource(MediaChannelId channelId);

	bool StartMulticast();

	void AddNotifyConnectedCallback(const NotifyConnectedCallback &callback);
	void AddNotifyDisconnectedCallback(const NotifyDisconnectedCallback &callback);

	std::string GetRtspUrlSuffix() const {
		return suffix;
	}

	void SetRtspUrlSuffix(std::string &suffix) {
		this->suffix = suffix;
	}

	std::string GetSdpMessage(std::string ip, std::string sessionName = "");

	MediaSource *GetMediaSource(MediaChannelId channelId);

	bool HandleFrame(MediaChannelId channelId, AVFrame frame);

	bool AddClient(SOCKET rtspFd, std::shared_ptr<RtpConnection> rtpConn);
	void RemoveClient(SOCKET rtspFd);

	MediaSessionId GetMediaSessionId() {
		return sessionId;
	}

	uint32_t GetNumClient() const {
		return (uint32_t)clientsMap.size();
	}

	bool IsMulticast() const {
		return isMulticast;
	}

	std::string GetMulticastIp() const {
		return multicastIp;
	}

	uint16_t GetMulticastPort(MediaChannelId channelId) const {
		if (channelId >= MAX_MEDIA_CHANNEL)
			return 0;

		return multicastPorts[channelId];
	}

private:
	friend class MediaSource;
	friend class RtspServer;
	MediaSession(std::string suffix);

	MediaSessionId sessionId = 0;
	std::string suffix;
	std::string sdp;

	std::vector<std::unique_ptr<MediaSource>> sources;
	std::vector<RingBuffer<AVFrame>> buffer;

	std::vector<NotifyConnectedCallback> connectedCbs;
	std::vector<NotifyDisconnectedCallback> disconnectedCbs;
	std::mutex mutex;
	std::mutex mapMutex;
	std::map<SOCKET, std::weak_ptr<RtpConnection>> clientsMap;

	bool isMulticast = false;
	uint16_t multicastPorts[MAX_MEDIA_CHANNEL];
	std::string multicastIp;
	std::atomic_bool hasNewClient;

	static std::atomic_uint lastSessionId;
};

class MulticastAddr {
public:
	static MulticastAddr &instance() {
		static MulticastAddr addr;
		return addr;
	}

	std::string GetAddr() {
		std::lock_guard<std::mutex> lock(mutex);
		std::string addrStr;
		struct sockaddr_in addr = { 0 };
		std::random_device rd;

		for (int n = 0; n <= 10; n++) {
			uint32_t range = 0xE8FFFFFF - 0xE8000100;
			addr.sin_addr.s_addr = htonl(0xE8000100 + (rd()) % range);
			addrStr = inet_ntoa(addr.sin_addr);

			if (addrs.find(addrStr) != addrs.end()) {
				addrStr.clear();
			} else {
				addrs.insert(addrStr);
				break;
			}
		}

		return addrStr;
	}

	void Release(std::string addr) {
		std::lock_guard<std::mutex> lock(mutex);
		addrs.erase(addr);
	}

private:
	std::mutex mutex;
	std::unordered_set<std::string> addrs;
};

}

#endif
