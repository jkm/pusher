#ifndef XOP_RTSP_MESSAGE_H
#define XOP_RTSP_MESSAGE_H

#include <cstring>
#include <string>
#include <unordered_map>
#include <utility>

#include "net/BufferReader.h"
#include "xop/media.h"
#include "xop/rtp.h"

namespace xop {

class RtspRequest {
public:
	enum Method {
		OPTIONS = 0,
		DESCRIBE,
		SETUP,
		PLAY,
		TEARDOWN,
		GET_PARAMETER,
		RTCP,
		NONE
	};

	const char *MethodToString[8] = {
		"OPTIONS",
		"DESCRIBE",
		"SETUP",
		"PLAY",
		"TEARDOWN",
		"GET_PARAMETER",
		"RTCP",
		"NONE"
	};

	enum RtspRequestParseState {
		kParseRequestLine,
		kParseHeadersLine,
		kParseBody,
		kGotAll,
	};

	bool ParseRequest(xop::BufferReader *buffer);

	bool GotAll() const {
		return state == kGotAll;
	}

	void Reset() {
		state = kParseRequestLine;
		requestLineParam.clear();
		headerLineParam.clear();
	}

	Method GetMethod() const {
		return method;
	}

	uint32_t GetCSeq() const;

	std::string GetRtspUrl() const;
	std::string GetRtspUrlSuffix() const;
	std::string GetIp() const;
	std::string GetAuthResponse() const;

	TransportMode GetTransportMode() const {
		return transport;
	}

	MediaChannelId GetChannelId() const {
		return channelId;
	}

	uint8_t GetRtpChannel() const;
	uint8_t GetRtcpChannel() const;

	uint16_t GetRtpPort() const;
	uint16_t GetRtcpPort() const;

	int BuildOptionRes(const char *buf, int bufSize);
	int BuildDescribeRes(const char *buf, int bufSize, const char *sdp);
	int BuildSetupMulticastRes(const char *buf, int bufSize, const char *multicastIp, uint16_t port, uint32_t sessionId);
	int BuildSetupTcpRes(const char *buf, int bufSize, uint16_t rtp_chn, uint16_t rtcp_chn, uint32_t sessionId);
	int BuildSetupUdpRes(const char *buf, int bufSize, uint16_t rtp_chn, uint16_t rtcp_chn, uint32_t sessionId);
	int BuildPlayRes(const char *buf, int bufSize, const char *rtp_info, uint32_t sessionId);
	int BuildTeardownRes(const char *buf, int bufSize, uint32_t sessionId);
	int BuildGetParamterRes(const char *buf, int bufSize, uint32_t sessionId);
	int BuildNotFoundRes(const char *buf, int bufSize);
	int BuildServerErrorRes(const char *buf, int bufSize);
	int BuildUnsupportedRes(const char *buf, int bufSize);
	int BuildUnauthorizedRes(const char *buf, int bufSize, const char *realm, const char *nonce);

private:
	bool ParseRequestLine(const char *begin, const char *end);
	bool ParseHeadersLine(const char *begin, const char *end);
	bool ParseCSeq(std::string &message);
	bool ParseAccept(std::string &message);
	bool ParseTransport(std::string &message);
	bool ParseSessionId(std::string &message);
	bool ParseMediaChannel(std::string &message);
	bool ParseAuthorization(std::string &message);

	Method method;
	MediaChannelId channelId;
	TransportMode transport;
	std::string authResponse;
	std::unordered_map<std::string, std::pair<std::string, uint32_t>> requestLineParam;
	std::unordered_map<std::string, std::pair<std::string, uint32_t>> headerLineParam;

	RtspRequestParseState state = kParseRequestLine;
};

class RtspResponse {
public:
	enum Method {
		OPTIONS = 0,
		DESCRIBE,
		ANNOUNCE,
		SETUP,
		RECORD,
		RTCP,
		NONE,
	};

	bool ParseResponse(xop::BufferReader *buffer);

	Method GetMethod() const {
		return method;
	}

	uint32_t GetCSeq() const {
		return cseq;
	}

	std::string GetSession() const {
		return session;
	}

	void SetUserAgent(const char *agent) {
		userAgent = std::string(agent);
	}

	void SetRtspUrl(const char *url) {
		rtspUrl = std::string(url);
	}

	int BuildOptionReq(const char *buf, int bufSize);
	int BuildDescribeReq(const char *buf, int bufSize);
	int BuildAnnounceReq(const char *buf, int bufSize, const char *sdp);
	int BuildSetupTcpReq(const char *buf, int bufSize, int channel);
	int BuildRecordReq(const char *buf, int bufSize);

private:
	Method method;
	uint32_t cseq = 0;
	std::string userAgent;
	std::string rtspUrl;
	std::string session;
};

}

#endif
