#ifndef XOP_VP8_SOURCE_H
#define XOP_VP8_SOURCE_H

#include "xop/MediaSource.h"
#include "xop/rtp.h"

namespace xop {

class VP8Source : public MediaSource {
public:
	static VP8Source *CreateNew(uint32_t frameRate = 25);
	~VP8Source();

	void SetFrameRate(uint32_t frameRate) {
		this->frameRate = frameRate;
	}

	uint32_t GetFrameRate() const {
		return frameRate;
	}

	virtual std::string GetMediaDescription(uint16_t port = 0);

	virtual std::string GetAttribute();

	virtual bool HandleFrame(MediaChannelId channelId, AVFrame frame);

	static uint32_t GetTimestamp();

private:
	VP8Source(uint32_t frameRate);

	uint32_t frameRate = 25;
};

}

#endif
