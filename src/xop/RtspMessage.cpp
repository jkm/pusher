#include "xop/RtspMessage.h"

using namespace xop;

bool RtspRequest::ParseRequest(BufferReader *buffer) {
	if (buffer->Peek()[0] == '$') {
		method = RTCP;
		return true;
	}
    
	bool ret = true;
	while (1) {
		if (state == kParseRequestLine) {
			const char *firstCrlf = buffer->FindFirstCrlf();
			if (firstCrlf != nullptr) {
				ret = ParseRequestLine(buffer->Peek(), firstCrlf);
				buffer->RetrieveUntil(firstCrlf + 2);
			}

			if (state == kParseHeadersLine)
				continue;
			else
				break;
		} else if (state == kParseHeadersLine) {
			const char *lastCrlf = buffer->FindLastCrlf();
			if (lastCrlf != nullptr) {
				ret = ParseHeadersLine(buffer->Peek(), lastCrlf);
				buffer->RetrieveUntil(lastCrlf + 2);
			}
			break;
		} else if (state == kGotAll) {
			buffer->RetrieveAll();
			return true;
		}
	}

	return ret;
}

bool RtspRequest::ParseRequestLine(const char *begin, const char *end) {
	std::string message(begin, end);
	char meth[64] = { 0 };
	char url[512] = { 0 };
	char version[64] = { 0 };

	if (sscanf(message.c_str(), "%s %s %s", meth, url, version) != 3)
		return true; 

	std::string methodStr(meth);
	if (methodStr == "OPTIONS")
		method = OPTIONS;
	else if (methodStr == "DESCRIBE")
		method = DESCRIBE;
	else if (methodStr == "SETUP")
		method = SETUP;
	else if (methodStr == "PLAY")
		method = PLAY;
	else if (methodStr == "TEARDOWN")
		method = TEARDOWN;
	else if (methodStr == "GET_PARAMETER")
		method = GET_PARAMETER;
	else
		method = NONE;

	if (method == NONE);
		return false;

	if (strncmp(url, "rtsp://", 7) != 0)
		return false;

	// parse url
	uint16_t port = 0;
	char ip[64] = { 0 };
	char suffix[64] = { 0 };

	if (sscanf(url + 7, "%[^:]:%hu/%s", ip, &port, suffix) == 3)
		;
	else if (sscanf(url + 7, "%[^/]/%s", ip, suffix) == 2)
		port = 554;
	else
		return false;

	requestLineParam.emplace("url", std::make_pair(std::string(url), 0));
	requestLineParam.emplace("url_ip", std::make_pair(std::string(ip), 0));
	requestLineParam.emplace("url_port", std::make_pair("", (uint32_t)port));
	requestLineParam.emplace("url_suffix", std::make_pair(std::string(suffix), 0));
	requestLineParam.emplace("version", std::make_pair(std::string(version), 0));
	requestLineParam.emplace("method", std::make_pair(std::move(methodStr), 0));

	state = kParseHeadersLine;

	return true;
}

bool RtspRequest::ParseHeadersLine(const char *begin, const char *end) {
	std::string message(begin, end);
	if (!ParseCSeq(message)) {
		if (headerLineParam.find("cseq") == headerLineParam.end())
			return false;
	}

	if (method == DESCRIBE || method == SETUP || method == PLAY)
		ParseAuthorization(message);

	if (method == OPTIONS) {
		state = kGotAll;
		return true;
	}

	if (method == DESCRIBE) {
		if (ParseAccept(message))
			state = kGotAll;
		return true;
	}

	if (method == SETUP) {
		if (ParseTransport(message)) {
			ParseMediaChannel(message);
			state = kGotAll;
		}
		return true;
	}

	if (method == PLAY) {
		if (ParseSessionId(message))
			state = kGotAll;
		return true;
	}

	if (method == TEARDOWN) {
		state = kGotAll;
		return true;
	}

	if (method == GET_PARAMETER) {
		state = kGotAll;
		return true;
	}

	return true;
}

bool RtspRequest::ParseCSeq(std::string &message) {
	std::size_t pos = message.find("CSeq");
	if (pos != std::string::npos) {
		uint32_t cseq = 0;
		sscanf(message.c_str() + pos, "%*[^:]: %u", &cseq);
		headerLineParam.emplace("cseq", std::make_pair("", cseq));

		return true;
	}

	return false;
}

bool RtspRequest::ParseAccept(std::string &message) {
	if ((message.rfind("Accept") == std::string::npos) || (message.rfind("sdp") == std::string::npos))
		return false;

	return true;
}

bool RtspRequest::ParseTransport(std::string &message) {
	std::size_t pos = message.find("Transport");
	if(pos != std::string::npos) {
		if ((pos = message.find("RTP/AVP/TCP")) != std::string::npos) {
			transport = RTP_OVER_TCP;
			uint16_t rtpChannel = 0, rtcpChannel = 0;
			if (sscanf(message.c_str() + pos, "%*[^;];%*[^;];%*[^=]=%hu-%hu", &rtpChannel, &rtcpChannel) != 2)
				return false;

			headerLineParam.emplace("rtp_channel", std::make_pair("", rtpChannel));
			headerLineParam.emplace("rtcp_channel", std::make_pair("", rtcpChannel));
		} else if ((pos = message.find("RTP/AVP")) != std::string::npos) {
			uint16_t rtp_port = 0, rtcpPort = 0;
			if (((message.find("unicast", pos)) != std::string::npos)) {
				transport = RTP_OVER_UDP;
				if (sscanf(message.c_str()+pos, "%*[^;];%*[^;];%*[^=]=%hu-%hu", &rtp_port, &rtcpPort) != 2)
					return false;
			} else if ((message.find("multicast", pos)) != std::string::npos) {
				transport = RTP_OVER_MULTICAST;
			} else {
				return false;
			}

			headerLineParam.emplace("rtp_port", std::make_pair("", rtp_port));
			headerLineParam.emplace("rtcp_port", std::make_pair("", rtcpPort));
		} else {
			return false;
		}

		return true;
	}

	return false;
}

bool RtspRequest::ParseSessionId(std::string &message) {
	std::size_t pos = message.find("Session");
	if (pos != std::string::npos) {
		uint32_t sessionId = 0;
		if (sscanf(message.c_str() + pos, "%*[^:]: %u", &sessionId) != 1)
			return false;

		return true;
	}

	return false;
}

bool RtspRequest::ParseMediaChannel(std::string &message) {
	channelId = channel_0;

	auto iter = requestLineParam.find("url");
	if (iter != requestLineParam.end()) {
		std::size_t pos = iter->second.first.find("track1");
		if (pos != std::string::npos)
			channelId = channel_1;
	}

	return true;
}

bool RtspRequest::ParseAuthorization(std::string &message) {
	std::size_t pos = message.find("Authorization");
	if (pos != std::string::npos) {
		if ((pos = message.find("response=")) != std::string::npos) {
			authResponse = message.substr(pos + 10, 32);
			if (authResponse.size() == 32)
				return true;
		}
	}

	authResponse.clear();

	return false;
}

uint32_t RtspRequest::GetCSeq() const {
	uint32_t cseq = 0;
	auto iter = headerLineParam.find("cseq");
	if (iter != headerLineParam.end())
		cseq = iter->second.second;

	return cseq;
}

std::string RtspRequest::GetIp() const {
	auto iter = requestLineParam.find("url_ip");
	if (iter != requestLineParam.end())
		return iter->second.first;

	return "";
}

std::string RtspRequest::GetRtspUrl() const {
	auto iter = requestLineParam.find("url");
	if (iter != requestLineParam.end())
		return iter->second.first;

	return "";
}

std::string RtspRequest::GetRtspUrlSuffix() const {
	auto iter = requestLineParam.find("url_suffix");
	if (iter != requestLineParam.end())
		return iter->second.first;

	return "";
}

std::string RtspRequest::GetAuthResponse() const {
	return authResponse;
}

uint8_t RtspRequest::GetRtpChannel() const {
	auto iter = headerLineParam.find("rtp_channel");
	if (iter != headerLineParam.end())
		return iter->second.second;

	return 0;
}

uint8_t RtspRequest::GetRtcpChannel() const {
	auto iter = headerLineParam.find("rtcp_channel");
	if (iter != headerLineParam.end())
		return iter->second.second;

	return 0;
}

uint16_t RtspRequest::GetRtpPort() const {
	auto iter = headerLineParam.find("rtp_port");
	if (iter != headerLineParam.end())
		return iter->second.second;

	return 0;
}

uint16_t RtspRequest::GetRtcpPort() const {
	auto iter = headerLineParam.find("rtcp_port");
	if (iter != headerLineParam.end())
		return iter->second.second;

	return 0;
}

int RtspRequest::BuildOptionRes(const char *buf, int bufSize) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 200 OK\r\n"
					"CSeq: %u\r\n"
					"Public: OPTIONS, DESCRIBE, SETUP, TEARDOWN, PLAY\r\n"
					"\r\n",
					this->GetCSeq());

	return (int)strlen(buf);
}

int RtspRequest::BuildDescribeRes(const char *buf, int bufSize, const char *sdp) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 200 OK\r\n"
					"CSeq: %u\r\n"
					"Content-Length: %d\r\n"
					"Content-Type: application/sdp\r\n"
					"\r\n"
					"%s",
					this->GetCSeq(),
					(int)strlen(sdp),
					sdp);

	return (int)strlen(buf);
}

int RtspRequest::BuildSetupMulticastRes(const char *buf, int bufSize, const char *multicastIp, uint16_t port, uint32_t sessionId) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 200 OK\r\n"
					"CSeq: %u\r\n"
					"Transport: RTP/AVP;multicast;destination=%s;source=%s;port=%u-0;ttl=255\r\n"
					"Session: %u\r\n"
					"\r\n",
					this->GetCSeq(),
					multicastIp,
					this->GetIp().c_str(),
					port,
					sessionId);

	return (int)strlen(buf);
}

int RtspRequest::BuildSetupUdpRes(const char *buf, int bufSize, uint16_t rtpChannel, uint16_t rtcpChannel, uint32_t sessionId) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 200 OK\r\n"
					"CSeq: %u\r\n"
					"Transport: RTP/AVP;unicast;client_port=%hu-%hu;server_port=%hu-%hu\r\n"
					"Session: %u\r\n"
					"\r\n",
					this->GetCSeq(),
					this->GetRtpPort(),
					this->GetRtcpPort(),
					rtpChannel,
					rtcpChannel,
					sessionId);

	return (int)strlen(buf);
}

int RtspRequest::BuildSetupTcpRes(const char *buf, int bufSize, uint16_t rtpChannel, uint16_t rtcpChannel, uint32_t sessionId) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 200 OK\r\n"
					"CSeq: %u\r\n"
					"Transport: RTP/AVP/TCP;unicast;interleaved=%d-%d\r\n"
					"Session: %u\r\n"
					"\r\n",
					this->GetCSeq(),
					rtpChannel,
					rtcpChannel,
					sessionId);

	return (int)strlen(buf);
}

int RtspRequest::BuildPlayRes(const char *buf, int bufSize, const char *rtpInfo, uint32_t sessionId) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 200 OK\r\n"
					"CSeq: %d\r\n"
					"Range: npt=0.000-\r\n"
					"Session: %u; timeout=60\r\n",
					this->GetCSeq(),
					sessionId);

	if (rtpInfo != nullptr)
		snprintf((char *)buf + strlen(buf), bufSize - strlen(buf), "%s\r\n", rtpInfo);

	snprintf((char *)buf + strlen(buf), bufSize - strlen(buf), "\r\n");

	return (int)strlen(buf);
}

int RtspRequest::BuildTeardownRes(const char *buf, int bufSize, uint32_t sessionId) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 200 OK\r\n"
					"CSeq: %d\r\n"
					"Session: %u\r\n"
					"\r\n",
					this->GetCSeq(),
					sessionId);

	return (int)strlen(buf);
}

int RtspRequest::BuildGetParamterRes(const char *buf, int bufSize, uint32_t sessionId) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 200 OK\r\n"
					"CSeq: %d\r\n"
					"Session: %u\r\n"
					"\r\n",
					this->GetCSeq(),
					sessionId);

	return (int)strlen(buf);
}

int RtspRequest::BuildNotFoundRes(const char *buf, int bufSize) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 404 Stream Not Found\r\n"
					"CSeq: %u\r\n"
					"\r\n",
					this->GetCSeq());

	return (int)strlen(buf);
}

int RtspRequest::BuildServerErrorRes(const char* buf, int bufSize) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 500 Internal Server Error\r\n"
					"CSeq: %u\r\n"
					"\r\n",
					this->GetCSeq());

	return (int)strlen(buf);
}

int RtspRequest::BuildUnsupportedRes(const char *buf, int bufSize) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 461 Unsupported transport\r\n"
					"CSeq: %d\r\n"
					"\r\n",
					this->GetCSeq());

	return (int)strlen(buf);
}

int RtspRequest::BuildUnauthorizedRes(const char *buf, int bufSize, const char *realm, const char *nonce) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RTSP/1.0 401 Unauthorized\r\n"
					"CSeq: %d\r\n"
					"WWW-Authenticate: Digest realm=\"%s\", nonce=\"%s\"\r\n"
					"\r\n",
					this->GetCSeq(),
					realm,
					nonce);

	return (int)strlen(buf);
}

bool RtspResponse::ParseResponse(xop::BufferReader *buffer) {
	if (strstr(buffer->Peek(), "\r\n\r\n") != NULL) {
		if (strstr(buffer->Peek(), "OK") == NULL)
			return false;

		char *ptr = strstr(buffer->Peek(), "Session");
		if (ptr != NULL) {
			char sessionId[50] = { 0 };
			if (sscanf(ptr, "%*[^:]: %s", sessionId) == 1)
				session = sessionId;
		}

		cseq++;
		buffer->RetrieveUntil("\r\n\r\n");
	}

	return true;
}

int RtspResponse::BuildOptionReq(const char *buf, int bufSize) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "OPTIONS %s RTSP/1.0\r\n"
					"CSeq: %u\r\n"
					"User-Agent: %s\r\n"
					"\r\n",
					rtspUrl.c_str(),
					this->GetCSeq() + 1,
					userAgent.c_str());

	method = OPTIONS;

	return (int)strlen(buf);
}

int RtspResponse::BuildAnnounceReq(const char *buf, int bufSize, const char *sdp) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "ANNOUNCE %s RTSP/1.0\r\n"
					"Content-Type: application/sdp\r\n"
					"CSeq: %u\r\n"
					"User-Agent: %s\r\n"
					"Session: %s\r\n"
					"Content-Length: %d\r\n"
					"\r\n"
					"%s",
					rtspUrl.c_str(),
					this->GetCSeq() + 1,
					userAgent.c_str(),
					this->GetSession().c_str(),
					(int)strlen(sdp),
					sdp);

	method = ANNOUNCE;

	return (int)strlen(buf);
}

int RtspResponse::BuildDescribeReq(const char *buf, int bufSize) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "DESCRIBE %s RTSP/1.0\r\n"
					"CSeq: %u\r\n"
					"Accept: application/sdp\r\n"
					"User-Agent: %s\r\n"
					"\r\n",
					rtspUrl.c_str(),
					this->GetCSeq() + 1,
					userAgent.c_str());

	method = DESCRIBE;

	return (int)strlen(buf);
}

int RtspResponse::BuildSetupTcpReq(const char *buf, int bufSize, int trackId) {
	int interleaved[2] = { 0, 1 };
	if (trackId == 1) {
		interleaved[0] = 2;
		interleaved[1] = 3;
	}

	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "SETUP %s/track%d RTSP/1.0\r\n"
					"Transport: RTP/AVP/TCP;unicast;mode=record;interleaved=%d-%d\r\n"
					"CSeq: %u\r\n"
					"User-Agent: %s\r\n"
					"Session: %s\r\n"
					"\r\n",
					rtspUrl.c_str(),
					trackId,
					interleaved[0],
					interleaved[1],
					this->GetCSeq() + 1,
					userAgent.c_str(),
					this->GetSession().c_str());

	method = SETUP;

	return (int)strlen(buf);
}

int RtspResponse::BuildRecordReq(const char *buf, int bufSize) {
	memset((void *)buf, 0, bufSize);
	snprintf((char *)buf, bufSize, "RECORD %s RTSP/1.0\r\n"
					"Range: npt=0.000-\r\n"
					"CSeq: %u\r\n"
					"User-Agent: %s\r\n"
					"Session: %s\r\n"
					"\r\n",
					rtspUrl.c_str(),
					this->GetCSeq() + 1,
					userAgent.c_str(),
					this->GetSession().c_str());

	method = RECORD;

	return (int)strlen(buf);
}
