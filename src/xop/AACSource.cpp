#include <chrono>
#include <cstdio>
#include <stdlib.h>
#include <sys/time.h>

#include "xop/AACSource.h"

using namespace xop;

AACSource::AACSource(uint32_t sampleRate, uint32_t channels, bool hasAdts) : sampleRate(sampleRate), channels(channels), hasAdts(hasAdts) {
	payload		= 97;
	mediaType	= AAC;
	clockRate	= sampleRate;
}

AACSource *AACSource::CreateNew(uint32_t sampleRate, uint32_t channels, bool hasAdts) {
	return new AACSource(sampleRate, channels, hasAdts);
}

AACSource::~AACSource() {
}

std::string AACSource::GetMediaDescription(uint16_t port) {
	char buf[100] = { 0 };
	sprintf(buf, "m=audio %hu RTP/AVP 97", port);

	return std::string(buf);
}

static uint32_t AACSampleRate[16] = {
	96000, 88200, 64000, 48000,
	44100, 32000, 24000, 22050,
	16000, 12000, 11025, 8000,
	7350, 0, 0, 0
};

std::string AACSource::GetAttribute() {
	char buf[500] = { 0 };
	sprintf(buf, "a=rtpmap:97 MPEG4-GENERIC/%u/%u\r\n", sampleRate, channels);

	uint8_t index = 0;
	for (index = 0; index < 16; index++) {
		if (AACSampleRate[index] == sampleRate)
			break;
	}

	if (index == 16)
		return "";

	uint8_t profile = 1;
	char config[10] = { 0 };

	sprintf(config, "%02x%02x", (uint8_t)((profile + 1) << 3) | (index >> 1), (uint8_t)((index << 7) | (channels << 3)));
	sprintf(buf + strlen(buf), "a=fmtp:97 profile-level-id=1;"
				   "mode=AAC-hbr;"
				   "sizelength=13;indexlength=3;indexdeltalength=3;"
				   "config=%04u",
				   atoi(config));

	return std::string(buf);
}

bool AACSource::HandleFrame(MediaChannelId channelId, AVFrame frame) {
	if (frame.size > (MAX_RTP_PAYLOAD_SIZE - AU_SIZE))
		return false;

	int adtsSize = 0;
	if (hasAdts)
		adtsSize = ADTS_SIZE;

	uint8_t *frameBuf = frame.buffer.get() + adtsSize;
	uint32_t frameSize = frame.size - adtsSize;

	char AU[AU_SIZE] = { 0 };
	AU[0] = 0x00;
	AU[1] = 0x10;
	AU[2] = (frameSize & 0x1fe0) >> 5;
	AU[3] = (frameSize & 0x1f) << 3;

	RtpPacket pkt;
	pkt.type = frame.type;
	pkt.timestamp = frame.timestamp;
	pkt.size = frameSize + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + AU_SIZE;
	pkt.last = 1;

	pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 0] = AU[0];
	pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 1] = AU[1];
	pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 2] = AU[2];
	pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 3] = AU[3];

	memcpy(pkt.data.get() + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + AU_SIZE, frameBuf, frameSize);

	if (cb)
		cb(channelId, pkt);

	return true;
}

uint32_t AACSource::GetTimestamp(uint32_t sampleRate) {
	// auto timePoint = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now());
	// return (uint32_t)(timePoint.time_since_epoch().count() * sampleRate / 1000);

	auto timePoint = std::chrono::time_point_cast<std::chrono::microseconds>(std::chrono::steady_clock::now());
	return (uint32_t)((timePoint.time_since_epoch().count() + 500) / 1000 * sampleRate / 1000);
}
