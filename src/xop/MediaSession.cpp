#include <cstring>
#include <ctime>
#include <forward_list>
#include <map>

#include "net/Logger.h"
#include "net/SocketUtil.h"
#include "xop/MediaSession.h"
#include "xop/RtpConnection.h"

using namespace xop;

std::atomic_uint MediaSession::lastSessionId(1);

MediaSession::MediaSession(std::string suffix) : suffix(suffix), sources(MAX_MEDIA_CHANNEL), buffer(MAX_MEDIA_CHANNEL) {
	hasNewClient = false;
	sessionId = ++lastSessionId;

	for (int n = 0; n < MAX_MEDIA_CHANNEL; n++)
		multicastPorts[n] = 0;
}

MediaSession *MediaSession::CreateNew(std::string suffix) {
	return new MediaSession(std::move(suffix));
}

MediaSession::~MediaSession() {
	if (multicastIp != "")
		MulticastAddr::instance().Release(multicastIp);
}

void MediaSession::AddNotifyConnectedCallback(const NotifyConnectedCallback &callback) {
	connectedCbs.push_back(callback);
}

void MediaSession::AddNotifyDisconnectedCallback(const NotifyDisconnectedCallback &callback) {
	disconnectedCbs.push_back(callback);
}

bool MediaSession::AddSource(MediaChannelId channelId, MediaSource *source) {
	source->SetSendFrameCallback([this](MediaChannelId channelId, RtpPacket pkt) {
		std::forward_list<std::shared_ptr<RtpConnection>> clientsList;
		std::map<int, RtpPacket> packets;
		{
			std::lock_guard<std::mutex> lock(mapMutex);
			for (auto iter = clientsMap.begin(); iter != clientsMap.end();) {
				auto conn = iter->second.lock();
				if (conn == nullptr) {
					clientsMap.erase(iter++);
				} else {
					int id = conn->GetId();
					if (id >= 0) {
						if (packets.find(id) == packets.end()) {
							RtpPacket tmp_pkt;
							memcpy(tmp_pkt.data.get(), pkt.data.get(), pkt.size);
							tmp_pkt.size = pkt.size;
							tmp_pkt.last = pkt.last;
							tmp_pkt.timestamp = pkt.timestamp;
							tmp_pkt.type = pkt.type;
							packets.emplace(id, tmp_pkt);
						}
						clientsList.emplace_front(conn);
					}
					iter++;
				}
			}
		}

		int count = 0;
		for (auto iter : clientsList) {
			int ret = 0;
			int id = iter->GetId();
			if (id >= 0) {
				auto iter2 = packets.find(id);
				if (iter2 != packets.end()) {
					count++;
					ret = iter->SendRtpPacket(channelId, iter2->second);
					if (isMulticast && ret == 0)
						break;
				}
			}
		}

		return true;
	});

	sources[channelId].reset(source);

	return true;
}

bool MediaSession::RemoveSource(MediaChannelId channelId) {
	sources[channelId] = nullptr;

	return true;
}

bool MediaSession::StartMulticast() {
	if (isMulticast)
		return true;

	multicastIp = MulticastAddr::instance().GetAddr();
	if (multicastIp == "")
		return false;

	std::random_device rd;
	multicastPorts[channel_0] = htons(rd() & 0xfffe);
	multicastPorts[channel_1] = htons(rd() & 0xfffe);

	isMulticast = true;

	return true;
}

std::string MediaSession::GetSdpMessage(std::string ip, std::string sessionName) {
	if (sdp != "")
		return sdp;

	if (sources.empty())
		return "";

	char buf[2048] = { 0 };

	snprintf(buf, sizeof(buf), "v=0\r\n"
				   "o=- 9%ld 1 IN IP4 %s\r\n"
				   "t=0 0\r\n"
				   "a=control:*\r\n",
				   (long)std::time(NULL),
				   ip.c_str());

	if (sessionName != "")
		snprintf(buf + strlen(buf), sizeof(buf) - strlen(buf), "s=%s\r\n", sessionName.c_str());

	if (isMulticast) {
		snprintf(buf + strlen(buf),
			 sizeof(buf) - strlen(buf),
			 "a=type:broadcast\r\n"
			 "a=rtcp-unicast: reflection\r\n");
	}

	for (uint32_t ch = 0; ch < sources.size(); ch++) {
		if (sources[ch]) {
			if (isMulticast) {
				snprintf(buf + strlen(buf),
					 sizeof(buf) - strlen(buf),
					 "%s\r\n",
					 sources[ch]->GetMediaDescription(multicastPorts[ch]).c_str());

				snprintf(buf + strlen(buf),
					 sizeof(buf) - strlen(buf),
					 "c=IN IP4 %s/255\r\n",
					 multicastIp.c_str());
			} else {
				snprintf(buf + strlen(buf),
					 sizeof(buf) - strlen(buf),
					 "%s\r\n",
					 sources[ch]->GetMediaDescription(0).c_str());
			}

			snprintf(buf + strlen(buf),
				 sizeof(buf) - strlen(buf),
				 "%s\r\n",
				 sources[ch]->GetAttribute().c_str());

			snprintf(buf + strlen(buf),
				 sizeof(buf) - strlen(buf),
				 "a=control:track%d\r\n", ch);
		}
	}

	sdp = buf;

	return sdp;
}

MediaSource *MediaSession::GetMediaSource(MediaChannelId channelId) {
	if (sources[channelId])
		return sources[channelId].get();

	return nullptr;
}

bool MediaSession::HandleFrame(MediaChannelId channelId, AVFrame frame) {
	std::lock_guard<std::mutex> lock(mutex);

	if (sources[channelId])
		sources[channelId]->HandleFrame(channelId, frame);
	else
		return false;

	return true;
}

bool MediaSession::AddClient(SOCKET rtspFd, std::shared_ptr<RtpConnection> rtpConn) {
	std::lock_guard<std::mutex> lock(mapMutex);

	auto iter = clientsMap.find(rtspFd);
	if (iter == clientsMap.end()) {
		std::weak_ptr<RtpConnection> rtpConnPtr = rtpConn;
		clientsMap.emplace(rtspFd, rtpConnPtr);

		for (auto &callback : connectedCbs)
			callback(sessionId, rtpConn->GetIp(), rtpConn->GetPort());

		hasNewClient = true;

		return true;
	}

	return false;
}

void MediaSession::RemoveClient(SOCKET rtspFd) {
	std::lock_guard<std::mutex> lock(mapMutex);

	auto iter = clientsMap.find(rtspFd);
	if (iter != clientsMap.end()) {
		auto conn = iter->second.lock();
		if (conn) {
			for (auto &callback : disconnectedCbs)
				callback(sessionId, conn->GetIp(), conn->GetPort());
		}
		clientsMap.erase(iter);
	}
}
