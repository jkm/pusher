#ifndef XOP_RTP_CONNECTION_H
#define XOP_RTP_CONNECTION_H

#include <cstdint>
#include <memory>
#include <random>
#include <string>
#include <vector>

#include "rtp.h"
#include "media.h"
#include "net/Socket.h"
#include "net/TcpConnection.h"

namespace xop {

class RtspConnection;

class RtpConnection {
public:
	RtpConnection(std::weak_ptr<TcpConnection> rtspConn);
	virtual ~RtpConnection();

	void SetClockRate(MediaChannelId channelId, uint32_t clockRate) {
		info[channelId].clock_rate = clockRate;
	}

	void SetPayloadType(MediaChannelId channelId, uint32_t payload) {
		info[channelId].rtp_header.payload = payload;
	}

	bool SetupRtpOverTcp(MediaChannelId channelId, uint16_t rtpChannel, uint16_t rtcpChannel);
	bool SetupRtpOverUdp(MediaChannelId channelId, uint16_t rtpPort, uint16_t rtcpPort);
	bool SetupRtpOverMulticast(MediaChannelId channelId, std::string ip, uint16_t port);

	uint32_t GetRtpSessionId() const {
		return (uint32_t)((size_t)(this));
	}

	uint16_t GetRtpPort(MediaChannelId channelId) const {
		return rtpPorts[channelId];
	}

	uint16_t GetRtcpPort(MediaChannelId channelId) const {
		return rtcpPorts[channelId];
	}

	SOCKET GetRtpSocket(MediaChannelId channelId) const {
		return rtpFds[channelId];
	}

	SOCKET GetRtcpSocket(MediaChannelId channelId) const {
		return rtcpFds[channelId];
	}

	std::string GetIp() {
		return ip;
	}

	uint16_t GetPort() {
		return port;
	}

	bool IsMulticast() const {
		return isMulticast;
	}

	bool IsSetup(MediaChannelId channelId) const {
		return info[channelId].isSetup;
	}

	std::string GetMulticastIp(MediaChannelId channelId) const;

	void Play();
	void Record();
	void Teardown();

	std::string GetRtpInfo(const std::string &url);
	int SendRtpPacket(MediaChannelId channelId, RtpPacket pkt);

	bool IsClosed() const {
		return isClosed;
	}

	int GetId() const;

	bool HasKeyFrame() const {
		return hasKeyFrame;
	}

private:
	friend class RtspConnection;
	friend class MediaSession;

	void SetFrameType(uint8_t frameType = 0);
	void SetRtpHeader(MediaChannelId channelId, RtpPacket pkt);

	int SendRtpOverTcp(MediaChannelId channelId, RtpPacket pkt);
	int SendRtpOverUdp(MediaChannelId channelId, RtpPacket pkt);

	std::weak_ptr<TcpConnection> rtspConn;

	std::string ip;
	uint16_t port;

	TransportMode transportMode;

	bool isMulticast = false;
	bool isClosed = false;
	bool hasKeyFrame = false;

	uint16_t rtpPorts[MAX_MEDIA_CHANNEL];
	uint16_t rtcpPorts[MAX_MEDIA_CHANNEL];

	SOCKET rtpFds[MAX_MEDIA_CHANNEL];
	SOCKET rtcpFds[MAX_MEDIA_CHANNEL];

	struct sockaddr_in addr;
	struct sockaddr_in rtpAddrs[MAX_MEDIA_CHANNEL];
	struct sockaddr_in rtcpAddrs[MAX_MEDIA_CHANNEL];

	MediaChannelInfo info[MAX_MEDIA_CHANNEL];
};

}

#endif
