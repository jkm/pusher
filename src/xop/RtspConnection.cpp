#include "net/SocketUtil.h"
#include "xop/MediaSession.h"
#include "xop/MediaSource.h"
#include "xop/RtspConnection.h"
#include "xop/RtspServer.h"

#define USER_AGENT "-_-"
#define RTSP_DEBUG 0
#define MAX_RTSP_MESSAGE_SIZE 2048

using namespace xop;

RtspConnection::RtspConnection(std::shared_ptr<Rtsp> rtsp, TaskScheduler *scheduler, SOCKET sockFd) : TcpConnection(scheduler, sockFd),
	rtspPtr(rtsp), scheduler(scheduler), rtpChannel(new Channel(sockFd)), request(new RtspRequest), response(new RtspResponse) {
	this->SetReadCallback([this](std::shared_ptr<TcpConnection> conn, xop::BufferReader &buffer) {
		return this->OnRead(buffer);
	});

	this->SetCloseCallback([this](std::shared_ptr<TcpConnection> conn) {
		this->OnClose();
	});

	aliveCount = 1;

	rtpChannel->SetReadCallback([this]() {
		this->HandleRead();
	});

	rtpChannel->SetWriteCallback([this]() {
		this->HandleWrite();
	});

	rtpChannel->SetCloseCallback([this]() {
		this->HandleClose();
	});

	rtpChannel->SetErrorCallback([this]() {
		this->HandleError();
	});

	for (int ch = 0; ch < MAX_MEDIA_CHANNEL; ch++)
		rtcpChannels[ch] = nullptr;
}

RtspConnection::~RtspConnection() {
}

bool RtspConnection::OnRead(BufferReader &buffer) {
	KeepAlive();

	int size = buffer.ReadableBytes();
	if (size <= 0)
		return false;

	if (mode == RTSP_SERVER) {
		if (!HandleRtspRequest(buffer))
			return false; 
	} else if (mode == RTSP_PUSHER) {
		if (!HandleRtspResponse(buffer))
			return false;
	}

	if (buffer.ReadableBytes() > MAX_RTSP_MESSAGE_SIZE)
		buffer.RetrieveAll();

	return true;
}

void RtspConnection::OnClose() {
	if (sessionId != 0) {
		auto rtsp = rtspPtr.lock();
		if (rtsp) {
			MediaSession::Ptr session = rtsp->LookMediaSession(sessionId);
			if (session)
				session->RemoveClient(this->GetSocket());
		}
	}

	for (int ch = 0; ch < MAX_MEDIA_CHANNEL; ch++) {
		if (rtcpChannels[ch] && !rtcpChannels[ch]->IsNoneEvent())
			scheduler->RemoveChannel(rtcpChannels[ch]);
	}
}

bool RtspConnection::HandleRtspRequest(BufferReader &buffer) {
#if RTSP_DEBUG
	std::string str(buffer.Peek(), buffer.ReadableBytes());
	if (str.find("rtsp") != std::string::npos || str.find("RTSP") != std::string::npos)
		std::cout << str << std::endl;
#endif

	if (request->ParseRequest(&buffer)) {
		RtspRequest::Method method = request->GetMethod();
		if (method == RtspRequest::RTCP) {
			HandleRtcp(buffer);
			return true;
		} else if (!request->GotAll()) {
			return true;
		}
        
		switch (method) {
			case RtspRequest::OPTIONS:
				HandleCmdOption();
				break;
			case RtspRequest::DESCRIBE:
				HandleCmdDescribe();
				break;
			case RtspRequest::SETUP:
				HandleCmdSetup();
				break;
			case RtspRequest::PLAY:
				HandleCmdPlay();
				break;
			case RtspRequest::TEARDOWN:
				HandleCmdTeardown();
				break;
			case RtspRequest::GET_PARAMETER:
				HandleCmdGetParamter();
				break;
			default:
				break;
		}

		if (request->GotAll())
			request->Reset();
	} else {
		return false;
	}

	return true;
}

bool RtspConnection::HandleRtspResponse(BufferReader &buffer) {
#if RTSP_DEBUG
	string str(buffer.Peek(), buffer.ReadableBytes());
	if (str.find("rtsp") != string::npos || str.find("RTSP") != string::npos)
		cout << str << endl;
#endif

	if (response->ParseResponse(&buffer)) {
		RtspResponse::Method method = response->GetMethod();
		switch (method) {
			case RtspResponse::OPTIONS:
				if (mode == RTSP_PUSHER)
					SendAnnounce();
				break;
			case RtspResponse::ANNOUNCE:
			case RtspResponse::DESCRIBE:
				SendSetup();
				break;
			case RtspResponse::SETUP:
				SendSetup();
				break;
			case RtspResponse::RECORD:
				HandleRecord();
				break;
			default:
				break;
		}
	} else {
		return false;
	}

	return true;
}

void RtspConnection::SendRtspMessage(std::shared_ptr<char> buf, uint32_t size) {
#if RTSP_DEBUG
	cout << buf.get() << endl;
#endif
	this->Send(buf, size);
	return;
}

void RtspConnection::HandleRtcp(BufferReader &buffer) {
	char *peek = buffer.Peek();
	if (peek[0] == '$' &&  buffer.ReadableBytes() > 4) {
		uint32_t pkt_size = peek[2] << 8 | peek[3];
		if (pkt_size + 4 >= buffer.ReadableBytes())
			buffer.Retrieve(pkt_size + 4);
	}
}
 
void RtspConnection::HandleRtcp(SOCKET sockFd) {
	char buf[1024] = { 0 };
	if (recv(sockFd, buf, 1024, 0) > 0)
		KeepAlive();
}

void RtspConnection::HandleCmdOption() {
	std::shared_ptr<char> res(new char[2048], std::default_delete<char[]>());
	int size = request->BuildOptionRes(res.get(), 2048);
	this->SendRtspMessage(res, size);	
}

void RtspConnection::HandleCmdDescribe() {
	if (conn == nullptr)
		conn.reset(new RtpConnection(shared_from_this()));

	int size = 0;
	std::shared_ptr<char> res(new char[4096], std::default_delete<char[]>());
	MediaSession::Ptr session = nullptr;

	auto rtsp = rtspPtr.lock();
	if (rtsp)
		session = rtsp->LookMediaSession(request->GetRtspUrlSuffix());

	if (!rtsp || !session) {
		size = request->BuildNotFoundRes(res.get(), 4096);
	} else {
		sessionId = session->GetMediaSessionId();
		session->AddClient(this->GetSocket(), conn);

		for (int ch = 0; ch < MAX_MEDIA_CHANNEL; ch++) {
			MediaSource *source = session->GetMediaSource((MediaChannelId)ch);
			if (source != nullptr) {
				conn->SetClockRate((MediaChannelId)ch, source->GetClockRate());
				conn->SetPayloadType((MediaChannelId)ch, source->GetPayload());
			}
		}

		std::string sdp = session->GetSdpMessage(SocketUtil::GetSocketIp(this->GetSocket()), rtsp->GetVersion());
		if (sdp == "")
			size = request->BuildServerErrorRes(res.get(), 4096);
		else
			size = request->BuildDescribeRes(res.get(), 4096, sdp.c_str());
	}

	SendRtspMessage(res, size);

	return;
}

void RtspConnection::HandleCmdSetup() {
	int size = 0;
	std::shared_ptr<char> res(new char[4096], std::default_delete<char[]>());
	MediaChannelId channelId = request->GetChannelId();
	MediaSession::Ptr session = nullptr;

	auto rtsp = rtspPtr.lock();
	if (rtsp)
		session = rtsp->LookMediaSession(sessionId);

	if (!rtsp || !session)
		goto server_error;

	if (session->IsMulticast()) {
		std::string multicast_ip = session->GetMulticastIp();
		if(request->GetTransportMode() == RTP_OVER_MULTICAST) {
			uint16_t port = session->GetMulticastPort(channelId);
			uint16_t sessionId = conn->GetRtpSessionId();
			if (!conn->SetupRtpOverMulticast(channelId, multicast_ip.c_str(), port))
				goto server_error;

			size = request->BuildSetupMulticastRes(res.get(), 4096, multicast_ip.c_str(), port, sessionId);
		} else {
			goto transport_unsupport;
		}
	} else {
		if (request->GetTransportMode() == RTP_OVER_TCP) {
			uint16_t rtpChannel = request->GetRtpChannel();
			uint16_t rtcpChannel = request->GetRtcpChannel();
			uint16_t sessionId = conn->GetRtpSessionId();

			conn->SetupRtpOverTcp(channelId, rtpChannel, rtcpChannel);
			size = request->BuildSetupTcpRes(res.get(), 4096, rtpChannel, rtcpChannel, sessionId);
		} else if (request->GetTransportMode() == RTP_OVER_UDP) {
			uint16_t peer_rtp_port = request->GetRtpPort();
			uint16_t peer_rtcp_port = request->GetRtcpPort();
			uint16_t sessionId = conn->GetRtpSessionId();

			if (conn->SetupRtpOverUdp(channelId, peer_rtp_port, peer_rtcp_port)) {
				SOCKET rtcpFd = conn->GetRtcpSocket(channelId);
				rtcpChannels[channelId].reset(new Channel(rtcpFd));
				rtcpChannels[channelId]->SetReadCallback([rtcpFd, this]() {
					this->HandleRtcp(rtcpFd);
				});
				rtcpChannels[channelId]->EnableReading();
				scheduler->UpdateChannel(rtcpChannels[channelId]);
			} else {
				goto server_error;
			}

			uint16_t serRtpPort = conn->GetRtpPort(channelId);
			uint16_t serRtcpPort = conn->GetRtcpPort(channelId);
			size = request->BuildSetupUdpRes(res.get(), 4096, serRtpPort, serRtcpPort, sessionId);
		} else {
			goto transport_unsupport;
		}
	}

	SendRtspMessage(res, size);
	return;

transport_unsupport:
	size = request->BuildUnsupportedRes(res.get(), 4096);
	SendRtspMessage(res, size);
	return;

server_error:
	size = request->BuildServerErrorRes(res.get(), 4096);
	SendRtspMessage(res, size);
	return ;
}

void RtspConnection::HandleCmdPlay() {
	if (conn == nullptr)
		return;

	state = START_PLAY;
	conn->Play();

	uint16_t sessionId = conn->GetRtpSessionId();
	std::shared_ptr<char> res(new char[2048], std::default_delete<char[]>());

	int size = request->BuildPlayRes(res.get(), 2048, nullptr, sessionId);
	SendRtspMessage(res, size);
}

void RtspConnection::HandleCmdTeardown() {
	if (conn == nullptr)
		return;

	conn->Teardown();

	uint16_t sessionId = conn->GetRtpSessionId();
	std::shared_ptr<char> res(new char[2048], std::default_delete<char[]>());
	int size = request->BuildTeardownRes(res.get(), 2048, sessionId);
	SendRtspMessage(res, size);

	//HandleClose();
}

void RtspConnection::HandleCmdGetParamter() {
	if (conn == nullptr)
		return;

	uint16_t sessionId = conn->GetRtpSessionId();
	std::shared_ptr<char> res(new char[2048], std::default_delete<char[]>());
	int size = request->BuildGetParamterRes(res.get(), 2048, sessionId);
	SendRtspMessage(res, size);
}

bool RtspConnection::HandleAuthentication() {
	return true;
}

void RtspConnection::SendOptions(ConnectionMode connMode) {
	if (conn == nullptr)
		conn.reset(new RtpConnection(shared_from_this()));

	auto rtsp = rtspPtr.lock();
	if (!rtsp) {
		HandleClose();
		return;
	}

	mode = connMode;
	response->SetUserAgent(USER_AGENT);
	response->SetRtspUrl(rtsp->GetRtspUrl().c_str());

	std::shared_ptr<char> req(new char[2048], std::default_delete<char[]>());
	int size = response->BuildOptionReq(req.get(), 2048);
	SendRtspMessage(req, size);
}

void RtspConnection::SendAnnounce() {
	MediaSession::Ptr session = nullptr;

	auto rtsp = rtspPtr.lock();
	if (rtsp)
		session = rtsp->LookMediaSession(1);

	if (!rtsp || !session) {
		HandleClose();
		return;
	} else {
		sessionId = session->GetMediaSessionId();
		session->AddClient(this->GetSocket(), conn);

		for (int ch = 0; ch < 2; ch++) {
			MediaSource *source = session->GetMediaSource((MediaChannelId)ch);
			if (source != nullptr) {
				conn->SetClockRate((MediaChannelId)ch, source->GetClockRate());
				conn->SetPayloadType((MediaChannelId)ch, source->GetPayload());
			}
		}
	}

	std::string sdp = session->GetSdpMessage(SocketUtil::GetSocketIp(this->GetSocket()), rtsp->GetVersion());
	if (sdp == "") {
		HandleClose();
		return;
	}

	std::shared_ptr<char> req(new char[4096], std::default_delete<char[]>());
	int size = response->BuildAnnounceReq(req.get(), 4096, sdp.c_str());
	SendRtspMessage(req, size);
}

void RtspConnection::SendDescribe() {
	std::shared_ptr<char> req(new char[2048], std::default_delete<char[]>());
	int size = response->BuildDescribeReq(req.get(), 2048);
	SendRtspMessage(req, size);
}

void RtspConnection::SendSetup() {
	int size = 0;
	std::shared_ptr<char> buf(new char[2048], std::default_delete<char[]>());
	MediaSession::Ptr session = nullptr;

	auto rtsp = rtspPtr.lock();
	if (rtsp)
		session = rtsp->LookMediaSession(sessionId);

	if (!rtsp || !session) {
		HandleClose();
		return;
	}

	if (session->GetMediaSource(channel_0) && !conn->IsSetup(channel_0)) {
		conn->SetupRtpOverTcp(channel_0, 0, 1);
		size = response->BuildSetupTcpReq(buf.get(), 2048, channel_0);
	} else if (session->GetMediaSource(channel_1) && !conn->IsSetup(channel_1)) {
		conn->SetupRtpOverTcp(channel_1, 2, 3);
		size = response->BuildSetupTcpReq(buf.get(), 2048, channel_1);
	} else {
		size = response->BuildRecordReq(buf.get(), 2048);
	}

	SendRtspMessage(buf, size);
}

void RtspConnection::HandleRecord() {
	state = START_PUSH;
	conn->Record();
}
