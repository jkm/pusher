#include <chrono>
#include <cstdio>
#include <sys/time.h>

#include "xop/VP8Source.h"

using namespace xop;

VP8Source::VP8Source(uint32_t frameRate) : frameRate(frameRate) {
	payload		= 96;
	clockRate	= 90000;
}

VP8Source *VP8Source::CreateNew(uint32_t frameRate) {
	return new VP8Source(frameRate);
}

VP8Source::~VP8Source() {
}

std::string VP8Source::GetMediaDescription(uint16_t port) {
	char buf[100] = { 0 };
	sprintf(buf, "m=video %hu RTP/AVP 96", port);

	return std::string(buf);
}

std::string VP8Source::GetAttribute() {
	return std::string("a=rtpmap:96 VP8/90000");
}

bool VP8Source::HandleFrame(MediaChannelId channelId, AVFrame frame) {
	uint8_t *frameBuf = frame.buffer.get();
	uint32_t frameSize = frame.size;

	if (frame.timestamp == 0)
		frame.timestamp = GetTimestamp();

	// X = R = N = 0; PartID = 0;
	// S = 1 if this is the first (or only) fragment of the frame
	uint8_t descriptor = 0x10;

	while (frameSize > 0) {
		uint32_t payloadSize = MAX_RTP_PAYLOAD_SIZE;

		RtpPacket pkt;
		pkt.type = frame.type;
		pkt.timestamp = frame.timestamp;
		pkt.size = RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + RTP_VPX_HEAD_SIZE + MAX_RTP_PAYLOAD_SIZE;
		pkt.last = 0;

		if (frameSize < MAX_RTP_PAYLOAD_SIZE) {
			payloadSize = frameSize;
			pkt.size = RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + RTP_VPX_HEAD_SIZE + frameSize;
			pkt.last = 1;
		}

		pkt.data.get()[RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + 0] = descriptor;
		memcpy(pkt.data.get() + RTP_TCP_HEAD_SIZE + RTP_HEADER_SIZE + RTP_VPX_HEAD_SIZE, frameBuf, payloadSize);

		if (cb) {
			if (!cb(channelId, pkt))
				return false;
		}

		frameBuf += payloadSize;
		frameSize -= payloadSize;

		descriptor = 0x00;
	}

	return true;
}

uint32_t VP8Source::GetTimestamp() {
	auto timePoint = std::chrono::time_point_cast<std::chrono::microseconds>(std::chrono::steady_clock::now());
	return (uint32_t)((timePoint.time_since_epoch().count() + 500) / 1000 * 90);
}
