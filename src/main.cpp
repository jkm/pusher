#include <errno.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include "config.h"
#include "v4l2/logger.h"
#include "v4l2/V4l2Capture.h"
#include "xop/RtspPusher.h"

int stop = 0;

void sighandler(int) {
	printf("SIGINT\n");
	stop = 1;
}

int main(int argc, char **argv) {
	std::shared_ptr<xop::EventLoop> loop(new xop::EventLoop());
	std::shared_ptr<xop::RtspPusher> pusher = xop::RtspPusher::Create(loop.get());

	xop::MediaSession *session = xop::MediaSession::CreateNew();
	session->AddSource(xop::channel_0, xop::H264Source::CreateNew());
	// session->AddSource(xop::channel_1, xop::AACSource::CreateNew(44100, 2, false));
	pusher->AddSession(session);

	if (pusher->OpenUrl(RTSP_URL, RTSP_TIMEOUT) < 0) {
		LOG(ERROR) << "Opening " << RTSP_URL << "failed";
		return 1;
	}

	V4L2DeviceParameters params(V4L2_DEV, V4L2_FORMAT, V4L2_WIDTH, V4L2_HEIGHT, V4L2_FPS, IOTYPE_MMAP, 1);
	V4l2Capture *capture = V4l2Capture::Create(params);

	if (capture == NULL) {
		LOG(ERROR) << "Opening " << V4L2_DEV << "failed";
		return 1;
	}

	signal(SIGINT, sighandler);

	while (!stop && pusher->IsConnected()) {
		if (capture->IsReadable()) {
			char buffer[capture->GetBufferSize()];
			int size = capture->Read(buffer, sizeof(buffer));
			if (size == -1) {
				LOG(NOTICE) << "stop " << strerror(errno);
				stop = 1;
			} else {
				xop::AVFrame frame = { 0 };
				frame.size = size;
				frame.timestamp = xop::H264Source::GetTimestamp();
				frame.buffer.reset(new uint8_t[frame.size]);
				memcpy(frame.buffer.get(), buffer, frame.size);
				pusher->PushFrame(xop::channel_0, frame);
			}
		} else {
			delete capture;
			capture = V4l2Capture::Create(params);
		}
	}

	if (capture)
		delete capture;

	return 0;
}
