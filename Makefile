BIN = pusher

CROSS_COMPILE =
CXX   = $(CROSS_COMPILE)g++
CC    = $(CROSS_COMPILE)gcc
STRIP = $(CROSS_COMPILE)strip

I_FLAGS	  = -Isrc
LD_FLAGS  = -lrt -pthread -lpthread -ldl -lm
CXX_FLAGS = -std=c++11

NET_SRC = $(wildcard ./src/net/*.cpp)
NET_OBJ = $(patsubst %.cpp,%.o,$(NET_SRC))

XOP_SRC = $(wildcard ./src/xop/*.cpp)
XOP_OBJ = $(patsubst %.cpp,%.o,$(XOP_SRC))

V4L2_SRC = $(wildcard ./src/v4l2/*.cpp)
V4L2_OBJ = $(patsubst %.cpp,%.o,$(V4L2_SRC))

MAIN_SRC = src/main.cpp
MAIN_OBJ = src/main.o


all: $(BIN)

$(BIN) : $(V4L2_OBJ) $(NET_OBJ) $(XOP_OBJ) $(MAIN_OBJ)
	$(CXX) $^ -o $@ $(CFLAGS) $(LD_FLAGS) $(CXX_FLAGS)

src/%.o : ./src/%.cpp
	$(CXX) -c $< -o $@ $(CXX_FLAGS) $(I_FLAGS)

src/v4l2/%.o : ./src/v4l2/%.cpp
	$(CXX) -c $< -o $@ $(CXX_FLAGS) $(I_FLAGS)

src/net/%.o : ./src/net/%.cpp
	$(CXX) -c $< -o $@ $(CXX_FLAGS) $(I_FLAGS)

src/xop/%.o : ./src/xop/%.cpp
	$(CXX) -c $< -o $@ $(CXX_FLAGS) $(I_FLAGS)

install:
	-install -m 755 $(BIN) /usr/local/bin

clean:
	-rm -rf $(V4L2_OBJ) $(NET_OBJ) $(XOP_OBJ) $(MAIN_OBJ) $(BIN)
