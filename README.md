# pusher
### RTSP Pusher for embedded Linux systems

Essentially, this project is a result of merging together the following ones:
- [RtspServer](https://github.com/PHZ76/RtspServer)
- [libv4l2cpp](https://github.com/mpromonet/libv4l2cpp)

with a few major changes:

- used `Makefile` instead of `CMakeLists.txt` (apparently libv4l2cpp [no longer](https://github.com/mpromonet/libv4l2cpp/commit/fac4b836b556f6befc55663f2aa1d6577fbabb8a) uses `Makefile`),
- removed log4cpp dependency (not needed when running on a small embedded system),
- removed digest (MD5) authentication (not needed in the properly secured local area network),
- cleaned up the codebase.

This project has been tested with [rtsp-simple-server](https://github.com/aler9/rtsp-simple-server).

### Usage:
This project has been created with simplicity in mind.
Therefore, building is as simple as running:
```
$ make && make install
```
Don't forget to adjust `src/config.h` file beforehand.

### Credits:
This project would *NOT* have been possible without the following people:

- [aler9](https://github.com/aler9)
- [PHZ76](https://github.com/PHZ76)
- [mpromonet](https://github.com/mpromonet)
